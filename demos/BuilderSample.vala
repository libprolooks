/* Builder in Vala sample code */

using Gtk;
using Prolooks;

public class BuilderSample {
    public BuilderSample () {
        try {
            var builder = new Builder ();
            builder.add_from_file ("example.glade");
            var window = builder.get_object ("window1") as Window;
            var led    = builder.get_object ("led1") as Led;
            window.show_all ();
            window.destroy.connect(Gtk.main_quit);
            Gtk.main ();
        } catch (Error e) {
            var msg = new MessageDialog (null, DialogFlags.MODAL,
                                         MessageType.ERROR, ButtonsType.CANCEL, 
                                         "Failed to load UI\n%s", e.message);
            msg.run ();
        } 
    }

    public static int main (string[] args) {
        Gtk.init (ref args);

        var sample = new BuilderSample ();
        return 0;
    }
}
