/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {


static int main (string[] args) {
    Gtk.init (ref args);
    var window = new Window (WindowType.TOPLEVEL);
    var widget = new Curve ();

    window.add (widget);
    window.destroy.connect(Gtk.main_quit);
    window.show_all ();
    Gtk.main ();
    return 0;
}


} // namespace Prolooks
