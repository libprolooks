/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {

public class TestDisplay : DisplayBase {
    public TestDisplay() {
         set_size_request (width, height);
    }

    protected override bool draw_contents(Cairo.Context cr) {
        string str = "123| 45| 67890";
        text(cr, str, 20, 20, 20);
        Cairo.TextExtents ext = Cairo.TextExtents();
        cr.text_extents (str, out ext);
        
        RGBA line_color = RGBA();
        line_color.parse (text_color_default);
        set_source_color (cr, line_color, 0.5);
        
        cr.move_to (20, 20 + ext.y_bearing);
        cr.rel_line_to (0, ext.height);
        cr.stroke();
        
        cr.move_to (20 + ext.x_advance + ext.x_bearing, 20 + ext.y_bearing);
        cr.rel_line_to (0, ext.height);
        cr.stroke();
        
        cr.move_to (3, 29);
        cr.rel_line_to (width - 20, 0);
        cr.stroke();
        return false;
    }
    
    private bool orig_size = true;

    public void toggle_size() {
        stdout.printf("orig_size: %d\n", (int)orig_size);
        int new_width, new_height;
        if (orig_size) {
            new_width  = 291;
            new_height = 49;
            orig_size = false;
        } else {
            new_width  = 383;
            new_height = 72;
            orig_size = true;
        }
        
        set_size_request (new_width, new_height);
    }
    
    public void toggle_matrix (ToggleButton button) {
        show_matrix = button.active;
    }

    public void toggle_glass_rim (ToggleButton button) {
        show_glass_rim = button.active;
    }


    static int main (string[] args) {
        Gtk.init (ref args);
        
        var window               = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
        var vbox                 = new Box(Gtk.Orientation.VERTICAL, 2);
        var resize_button        = new Button.with_label("Resize");
        var matrix_button        = new ToggleButton.with_label("Show Matrix");
        var glass_rim_button     = new ToggleButton.with_label("Show glass rim");
        var alignment            = new Gtk.Alignment (0.5f, 0.5f, 0.5f, 0.5f);
        TestDisplay display      = new TestDisplay ();
        
        alignment.bottom_padding = 10;
        alignment.top_padding    = 10;
        alignment.left_padding   = 10;
        alignment.right_padding  = 10;
        alignment.add (display);
        alignment.set_size_request(display.width + 20, display.height + 20);
        
        resize_button.pressed.connect(display.toggle_size);
        resize_button.set_size_request(100, 25);
        
        matrix_button.active = true;
        matrix_button.toggled.connect(display.toggle_matrix);

        glass_rim_button.active = true;
        glass_rim_button.toggled.connect(display.toggle_glass_rim);
        
        vbox.add (alignment);
        vbox.add (resize_button);
        vbox.add (matrix_button);
        vbox.add (glass_rim_button);
        
        
        window.add (vbox);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }
}

} // namespace prolooks
