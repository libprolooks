/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    public class GaussianBlurDemo : DrawingArea {
    
        public GaussianBlurDemo () {
            // Set favored widget size
            set_size_request (100, 100);
			draw.connect(on_draw);
        }
                
        /* Widget is asked to draw itself */
		public bool on_draw (Cairo.Context cr) {   
			Gtk.Allocation allocation;
			get_allocation(out allocation);     

			SimpleKnobImageSource knob = new SimpleKnobImageSource();

			double knob_width  = knob.get_knob_width ();
			double knob_height = knob.get_knob_height ();

			Cairo.ImageSurface surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, (int) knob_width, (int) knob_height);
			Cairo.Context knobcr = new Cairo.Context (surface);
			knobcr.set_source_rgba (1, 1, 1, 0.0);
			knobcr.paint ();
			knob.paint_knob (knobcr, KnobMode.BIPOLAR, 0, knob.get_line_width(), knob.get_radius(), knob_width / 2, knob_height / 2);

			cr.set_source_rgb (0, 0, 0);
			
//			Gdk.Pixbuf pixbuf = cairo_image_surface_to_pixbuf (surface);
//			draw_pixbuf_with_shadow (cr, pixbuf, allocation.width/2 - knob_width/2, allocation.height/2 - knob_height/2, knob_width, knob_height, 0, 3);
			draw_image_surface_with_shadow (cr, surface, allocation.width/2 - knob_width/2, allocation.height/2 - knob_height/2, knob_width, knob_height, 0, 3);
            
            return false;
        }
    }

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        
        var gaussianBlurDemo = new GaussianBlurDemo();

        window.add (gaussianBlurDemo);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }
} // namespace Prolooks
