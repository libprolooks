/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    public class GlossDemo : DrawingArea {
    
        public GlossDemo () {

            // Set favored widget size
            set_size_request (500, 55);
			draw.connect(on_draw);
        }
                
        /* Widget is asked to draw itself */
		public bool on_draw (Cairo.Context cr) {   
			Gtk.Allocation allocation;
			get_allocation(out allocation);     

            set_source_color_string(cr, "#EDEDED");
            set_source_color(cr, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]));
            cr.rectangle(0, 0, allocation.width, allocation.height);
            cr.fill();

			double padding = 5.0;
			double glossbar_height = allocation.height - 2 * padding;
			double glossbar_width  = (allocation.width - 2 * padding) / 5.0 ;

            Cairo.Pattern gloss_red    = gloss_gradient_pattern(0.0, padding, 0.0, glossbar_height + padding, rgba_from_string("#D84212"), 50);
			Cairo.Pattern gloss_green  = gloss_gradient_pattern(0.0, padding, 0.0, glossbar_height + padding, rgba_from_string("#237000"), 50);
			Cairo.Pattern gloss_blue   = gloss_gradient_pattern(0.0, padding, 0.0, glossbar_height + padding, rgba_from_string("#0081FF"), 50);
			Cairo.Pattern gloss_violet = gloss_gradient_pattern(0.0, padding, 0.0, glossbar_height + padding, rgba_from_string("#9331AB"), 50);
			Cairo.Pattern gloss_gray   = gloss_gradient_pattern(0.0, padding, 0.0, glossbar_height + padding, rgba_from_string("#5B5B57"), 50);
			
            cr.rectangle(padding, padding,  glossbar_width, glossbar_height);
            cr.set_source(gloss_blue);
            cr.fill();

            cr.rectangle(padding + glossbar_width, padding,  glossbar_width, glossbar_height);
            cr.set_source(gloss_red);
            cr.fill();

            cr.rectangle(padding + 2.0 * glossbar_width, padding,  glossbar_width, glossbar_height);
            cr.set_source(gloss_green);
            cr.fill();

            cr.rectangle(padding + 3.0 * glossbar_width, padding,  glossbar_width, glossbar_height);
            cr.set_source(gloss_violet);
            cr.fill();

            cr.rectangle(padding + 4.0 * glossbar_width, padding,  glossbar_width, glossbar_height);
            cr.set_source(gloss_gray);
            cr.fill();
            
            return false;
        }
    }

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        
        var glossDemo = new GlossDemo();

        window.add (glossDemo);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }
} // namespace Prolooks
