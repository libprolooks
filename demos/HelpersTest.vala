/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {    
    public static void helpers_unit_tests () {
        Test.add_func ("/prolooks/helpers/rgba_from_string", () => {
               RGBA red = rgba_from_string("#ff0000");
               assert (red.red == 1.0);
               assert (red.green == 0);
               assert (red.blue == 0);
            }
        );

        Test.add_func ("/prolooks/helpers/color_to_string", () => {
               string original = "rgb(99,111,222)";
               RGBA color = rgba_from_string(original);
	       string result = color.to_string();
	       stdout.printf(result);
               assert (result == original);

               original = "rgb(10,16,5)";
               color = rgba_from_string(original);
	       result = color.to_string();
	       stdout.printf(result);
               assert (result == original);
            }
        );

        Test.add_func ("/prolooks/helpers/rgb2hsv <-> hsv2rgb", () => {
               RGBA color = rgba_from_string ("rgb(255, 0, 0)");
               HSV hsv = new HSV ();
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 0.0);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 1.0);
               assert (color.green == 0);
               assert (color.blue == 0);
               
               color = rgba_from_string ("#00ff00");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 120.0);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 0);
               assert (color.green == 1.0);
               assert (color.blue == 0);

               color = rgba_from_string ("#0000ff");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 240.0);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 0);
               assert (color.green == 0);
               assert (color.blue == 1.0);
               
               color = rgba_from_string ("#ff00ff");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 300.0);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 1.0);
               assert (color.green == 0);
               assert (color.blue == 1.0);
               
               color = rgba_from_string ("#800080");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 300.0);
               assert (hsv.saturation == 1.0);
               assert (Math.floor(hsv.value * 10.0) / 10.0 == 0.5);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
	       assert (color.to_string() == "rgb(128,0,128)");
               
               color = rgba_from_string ("#ffff00");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 60.0);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 1.0);
               assert (color.green == 1.0);
               assert (color.blue == 0);
               
               color = rgba_from_string ("#00ffff");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 180.0);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 0);
               assert (color.green == 1.0);
               assert (color.blue == 1.0);               

               color = rgba_from_string ("#00ECFF");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (Math.floor(hsv.hue) == 184);
               assert (hsv.saturation == 1.0);
               assert (hsv.value == 1.0);

               /* rounding errors here
               color = hsv.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 0);
               assert (color.green == 61016);
               assert (color.blue == 1.0);                              
               */
               
               color = rgba_from_string("#ffffff");
               hsv.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsv: %s\n", color.to_string(), hsv.to_string());
               assert (hsv.hue == 0.0);
               assert (hsv.saturation == 0.0);
               assert (hsv.value == 1.0);
               
            }
        );

        Test.add_func ("/prolooks/helpers/rgb2hsl <-> hsl2rgb", () => {
               RGBA color = rgba_from_string ("#ff0000");
               HSL hsl = new HSL ();
               hsl.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsl: %s\n", color.to_string(), hsl.to_string());
               assert (hsl.hue == 0.0);
               assert (hsl.saturation == 1.0);
               assert (hsl.lightness == 0.5);

               color = hsl.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 1.0);
               assert (color.green == 0);
               assert (color.blue == 0);
               
               color = rgba_from_string ("#00ff00");
               hsl.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsl: %s\n", color.to_string(), hsl.to_string());
               assert (hsl.hue == 120.0);
               assert (hsl.saturation == 1.0);
               assert (hsl.lightness == 0.5);

               color = hsl.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 0);
               assert (color.green == 1.0);
               assert (color.blue == 0);

               color = rgba_from_string ("#0000ff");
               hsl.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsl: %s\n", color.to_string(), hsl.to_string());
               assert (hsl.hue == 240.0);
               assert (hsl.saturation == 1.0);
               assert (hsl.lightness == 0.5);

               color = hsl.to_rgba ();
               stdout.printf ("Converting back to RGB yields:  %s\n", color.to_string());
               assert (color.red == 0);
               assert (color.green == 0);
               assert (color.blue == 1.0);
               
               
               color = rgba_from_string("#ffffff");
               hsl.from_rgba (color);
               stdout.printf ("\ncolor %s is in hsl: %s\n", color.to_string(), hsl.to_string());
               assert (hsl.hue == 0.0);
               assert (hsl.saturation == 0.0);
               assert (hsl.lightness == 1.0);
               
            }
        );

        Test.add_func ("/prolooks/helpers/shade", () => {
               RGBA gray = rgba_from_string("#0a0a0a");
               RGBA shaded = rgba_shade (gray, 1.0);
               stdout.printf ("\noriginal color: %s\n", gray.to_string());
               stdout.printf ("shaded color: %s\n", shaded.to_string());
               assert (strcmp(shaded.to_string(), "rgb(10,10,10)") == 0);

               shaded = rgba_shade (gray, 0.5);
               stdout.printf ("\noriginal color: %s\n", gray.to_string());
               stdout.printf ("shaded color: %s\n", shaded.to_string());
               assert (strcmp(shaded.to_string(), "rgb(5,5,5)") == 0);
            }
        );
    }
    
    static int main (string[] args) {
        Test.init (ref args);
        helpers_unit_tests ();
        Test.run ();
        return 0;
    }

} // namespace Prolooks
