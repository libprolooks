/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {


static int main (string[] args) {
    Gtk.init (ref args);
    var window = new Window (WindowType.TOPLEVEL);
    var keyboard = new Keyboard ();
    
    keyboard.interactive = true;

    keyboard.pre_draw += (self, c, ki) => {
        if (ki.note == 60) c.set_source_rgb(1.0, 1.0, 0.5); 
        return false; 
    };

    keyboard.post_draw += (self, c, ki) => { 
        if (ki.note % 12 != 0 && ki.note % 12 != 3 && ki.note % 12 != 7)
            return;
        c.rectangle(ki.x + ki.width / 2 - 2, ki.y + ki.height - 8, 4, 4);
        if (ki.black)
            c.set_source_rgb(1.0, 1.0, 1.0);
        else
            c.set_source_rgb(0.0, 0.0, 0.0);
        c.fill(); 
    };

    keyboard.note_on += (self, note, vel) => { 
        message("note on %d %d", note, vel); 
    };

    keyboard.note_off += (self, note) => { 
        message("note off %d", note); 
    };

    window.add (keyboard);
    window.destroy.connect(Gtk.main_quit);
    window.show_all ();
    Gtk.main ();
    return 0;
}


} // namespace Prolooks
