/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        var hbox = new Box (Gtk.Orientation.HORIZONTAL, 0);
        for (int i = 0; i <= (int)KnobMode.ENDLESS * 2 + 1; i++) {
            Knob knob = new Knob ();
            knob.knob_mode = (KnobMode)i % (int)KnobMode.ENDLESS;
			if (i < 2) {
				var isource = new SimpleKnobImageSource ();
				if (i == 0)
					isource.led_color = rgba_from_string ("#ff0000");
				else
					isource.led_color = rgba_from_string ("#ffff00");
				knob.image_source = isource;
			}
            hbox.pack_start (knob);
        }
		
		Knob thorwil_knob = new Knob ();
		var isource = new ThorwilKnobImageSource ();
		isource.lamp_color = rgba_from_string ("#9fc717");
		thorwil_knob.image_source = isource;
		hbox.pack_start (thorwil_knob);

        window.add (hbox);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
