/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        
        var hbox = new Box (Gtk.Orientation.HORIZONTAL, 0);
        var knob = new KnobWithDisplay ();
		knob.decimal_places = 1;
        hbox.pack_end (knob);

		knob = new KnobWithDisplay ();
		knob.decimal_places = 1;
        hbox.pack_end (knob);

        window.add (hbox);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
