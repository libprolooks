/* 
    Copyright 2009 by Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static int main (string[] args) {
        int i, j;
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        var table = new Table (10, 10, true);
        for (i = 0; i < 10; i++)
        {
            for (j = 0; j < 10; j++)
            {
                var widget = new Led();
                if ((i & 2) != 0)
                    widget.set_rgb(i & 1, 1, 0);
                else
                    widget.set_rgb(i & 1, 0.5f * (1 - (i & 1)), 1 - (i & 1));
                table.attach_defaults(widget, i, i + 1, j, j+1);
                widget.led_state = 0 != ((i ^ j) & 1);
            }
        }
        window.add (table);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
