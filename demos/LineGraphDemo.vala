/* 
    Copyright 2009 by Hans Baier, Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        var hbox = new Box (Gtk.Orientation.HORIZONTAL, 5);
        
        hbox.pack_start(new LineGraph ());
        hbox.pack_start(new LineGraph ());
        
        window.add (hbox);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
