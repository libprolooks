/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

// template for new widgets

namespace Prolooks {
    
    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
        var widget = new SmallButton ();
        widget.color = "#10a010";
        window.add (widget);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
