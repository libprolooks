using Gtk;
using Gdk;

namespace Prolooks {
    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
        var widget = new StepEditor ();
        widget.steps = 16;
        for (int i = 0; i < 16; i++)
            widget.set_step(i, (float)(i / 15.0));
    
        window.add (widget);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
