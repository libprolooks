/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        ThorwilKnobImageSource image = new ThorwilKnobImageSource ();
		/*
		image.knob_width = 210;
		image.knob_height = 210;
		image.set_size_request ((int) (image.knob_width * image.phases), (int)image.knob_height);
		*/
        window.add (image);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
