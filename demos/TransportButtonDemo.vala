/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {

public class TransportButtons : TransportButton {

    public TransportButtons () {
        // Set favored widget size
        set_size_request (280, 40);
		draw.connect(on_draw);
    }
    
        /* Widget is asked to draw itself */
	public new bool on_draw (Cairo.Context cr) {   
		Gtk.Allocation allocation;
		get_allocation(out allocation);     

        cr.translate (10.0, 10.0);
        paint_stop_icon (cr);

        cr.translate (20.0, 0.0);
        paint_play_icon (cr);
        
        cr.translate (20.0, 0.0);
        paint_rec_icon (cr);
        
        cr.translate (20.0, 0.0);
        paint_fast_forward_icon (cr);
                
        cr.translate (20.0, 0.0);
        paint_fast_to_end_icon (cr);

        cr.translate (20.0, 0.0);
        paint_fast_backward_icon (cr);

        cr.translate (20.0, 0.0);
        paint_fast_to_start_icon (cr);
        

        return false;
    }
          
    static int main (string[] args) {
            Gtk.init (ref args);
            var window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
            var vbox   = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            var hbox   = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
            var widget = new TransportButtons ();
            vbox.add (widget);

            var button = new TransportButton();
            button.icon_type = IconType.STOP;
            hbox.add (button);
            button = new TransportButton();
            button.icon_type = IconType.PLAY;
            hbox.add (button);
            button = new TransportButton();
            button.icon_type = IconType.REC;
            hbox.add (button);
            button = new TransportButton();
            button.icon_type = IconType.FAST_BACKWARD;
            hbox.add (button);
            button = new TransportButton();
            button.icon_type = IconType.FAST_FORWARD;
            hbox.add (button);
            button = new TransportButton();
            button.icon_type = IconType.TO_START;
            hbox.add (button);
            button = new TransportButton();
            button.icon_type = IconType.TO_END;
            hbox.add (button);
            
            vbox.add (hbox);

            int[] sizes = {16, 22, 24, 28, 32, 48};
            foreach (var size in sizes) {
                hbox   = new Gtk.HBox (true, 100);

                button = new TransportButton();
                button.icon_type = IconType.STOP;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                button = new TransportButton();
                button.icon_type = IconType.PLAY;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                button = new TransportButton();
                button.icon_type = IconType.REC;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                button = new TransportButton();
                button.icon_type = IconType.FAST_BACKWARD;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                button = new TransportButton();
                button.icon_type = IconType.FAST_FORWARD;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                button = new TransportButton();
                button.icon_type = IconType.TO_START;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                button = new TransportButton();
                button.icon_type = IconType.TO_END;
                button.set_size_request (size, size);
                hbox.add (button);
                hbox.set_child_packing (button, false, false, 0, PackType.END);
                
                vbox.add (hbox);
            }

            window.add (vbox);
          
            window.destroy.connect(Gtk.main_quit);
            window.show_all ();
            Gtk.main ();
            return 0;
        }
}

} // namespace Prolooks
