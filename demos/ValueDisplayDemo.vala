/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Window (WindowType.TOPLEVEL);
        
        var vbox = new Box (Gtk.Orientation.VERTICAL, 0);
        Knob knob = new Knob ();
        vbox.add (knob);
        var display = new ValueDisplay ();
        vbox.pack_end (display);
        knob.user_data = display;
        knob.value_changed += (knob) => {
            ValueDisplay disp = knob.user_data as ValueDisplay;
            disp.text = (knob.get_value () * 10000).to_string ();
        };
        window.add (vbox);
        window.destroy.connect(Gtk.main_quit);
        window.show_all ();
        Gtk.main ();
        return 0;
    }

} // namespace Prolooks
