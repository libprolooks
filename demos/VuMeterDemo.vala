/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

private VuMeter widget;

bool animate() {
    double val = GLib.Random.next_double();
    widget.value = val;
    return true;
}

static int main (string[] args) {
    Gtk.init (ref args);
    var window = new Window (WindowType.TOPLEVEL);
    widget = new VuMeter ();

    GLib.Timeout.add(100, animate);
    window.add (widget);
    window.destroy.connect(Gtk.main_quit);
    window.show_all ();
    Gtk.main ();
    return 0;
}


} // namespace Prolooks
