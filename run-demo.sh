#!/bin/bash
DIR=demos
DEMOS=$(cd $DIR; echo *Demo)

if test "$*" == ""; then 
RESULTS=$(zenity --height=450 --list --text="Please choose" --checklist --column Choose --column Demo --print-column 2 --separator=" " $(\
    set -i N;\
    N=1; \
    for i in $DEMOS; \
    do \
        echo -n $N $i " ";\
        N=$[N+1];\
    done))
else
    RESULTS="$*"
fi

cd demos
for i in $RESULTS; do ../$DIR/$i & done
cd ..
