/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {

public class ButtonBase : DrawingArea {
    /* See bugzilla 629593 */
    private int bug_work_around_for_lock = 0;
    protected ButtonState button_state = ButtonState.NORMAL;
    protected ButtonType  button_type  = ButtonType.PRESS_BUTTON;
    protected bool        prelighted;

    /* See bugzilla 629593 */
    protected void reference_private_var() {
        bug_work_around_for_lock=1;
    }
    
    public signal void toggled (bool active);
    public signal void pressed  ();
    public signal void released ();

    [Description(nick="active", blurb="Whether the button is pressed or not (usually only makes sense for toggle buttons like rec or play)")]
    public bool active { 
        get {
            bool result = false;
            lock (button_state) {
                result = button_state == ButtonState.PRESSED;
            }
            return result;
        } 
        set {
            lock (button_state) {
                if (value && button_type == ButtonType.TOGGLE_BUTTON) {
                    button_state = ButtonState.PRESSED;
                    queue_draw ();
                } else {
                    button_state = ButtonState.NORMAL;
                    queue_draw ();
                }
                
                if (button_type == ButtonType.TOGGLE_BUTTON) {
                    toggled (this.active);
                } else {
                    pressed  ();
                    released ();
                }
            }
        }
    }
    
    public void set_active_no_signals (bool active) {
        if (active) {
            lock (button_state) {
                button_state = ButtonState.PRESSED;
            }
        } else {
            lock (button_state) {
                button_state = ButtonState.NORMAL;
            }
        }
        queue_draw ();
    }

    
    
    protected void toggle_state () {
        if (button_state == ButtonState.PRESSED) {
            button_state = ButtonState.NORMAL;
        } else {
            button_state = ButtonState.PRESSED;
        }
    }
    
    /* Mouse button got pressed over widget */
    public override bool button_press_event (Gdk.EventButton event) {
        if (event.button == 1) {
            // workaround: double event sent, if clicks are fast
            if (event.type != EventType.BUTTON_PRESS) {
                return false;
            }
            
            if (button_type == ButtonType.TOGGLE_BUTTON) {
                lock (button_state) {
                    toggle_state ();
                    toggled (this.active);
                }
            } else {
                lock (button_state) {
                    button_state = ButtonState.PRESSED;
                    pressed ();
                }
            }
            
            queue_draw ();
        }
        return true;
    }

    /* Mouse button got released */
    public override bool button_release_event (Gdk.EventButton event) {
        if (event.button == 1) {
            if (button_type == ButtonType.PRESS_BUTTON) {
                lock (button_state) {
                   button_state = ButtonState.NORMAL;
                   released ();
                }
            }
            queue_draw ();
        }
        return true;
    }

    public override bool enter_notify_event (Gdk.EventCrossing event) {
        prelighted = true;
        queue_draw ();
        
        return true;
    }

    public override bool leave_notify_event (Gdk.EventCrossing event) {
        prelighted = false;
        queue_draw ();

        return true;
    }    
}

} // namespace Prolooks
