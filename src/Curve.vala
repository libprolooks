/* 
    Copyright 2009 by Hans Baier, Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

public class CurvePoint {
    public float x {get;set;}
    public float y {get;set;}
    public CurvePoint (float x, float y) {
        this.x = x;
        this.y = y;
    }
}
        
public class Curve : DrawingArea {
    /// Called when a point has been edited, added or removed
    public signal void curve_changed (ref List<CurvePoint> data);
    
    /// Called to clip/snap/otherwise adjust candidate point coordinates
    public signal void clipping (int pt, ref float x, ref float y, ref bool hide) ;

    /// Array of points
    List<CurvePoint> points;
    
    /// Coordinate ranges (in logical coordinates for top left and bottom right)
    float x0;
	float y0;
	float x1;
	float y1;
    
    /// Currently selected point (when dragging/adding), or -1 if none is selected
    int cur_pt;
    
    /// If currently selected point is a candidate for deletion (ie. outside of graph+margin range)
    bool hide_current;
    
    /// Cached hand (drag) cursor
    Gdk.Cursor hand_cursor;
    
    /// Cached pencil (add point) cursor
    Gdk.Cursor pencil_cursor;
    
    /// Cached arrow (do not add point) cursor
    Gdk.Cursor arrow_cursor;
    
    /// Maximum number of points
    [Description(nick="point limit", blurb="the maximum number of curve points that can be added")]
    public uint point_limit { get; set; }

    construct {
        this.point_limit = 10;
        points = new List<CurvePoint> ();
        points.append (new CurvePoint (0.0f, 1.0f));
        points.append (new CurvePoint (1.0f, 1.0f));
        x0 = 0.0f;
        x1 = 1.0f;
        y0 = 1.0f;
        y1 = 0.0f;
        cur_pt = -1;
        hide_current = false;
        pencil_cursor = new Gdk.Cursor (Gdk.CursorType.PENCIL);
        hand_cursor   = new Gdk.Cursor (Gdk.CursorType.FLEUR);
        arrow_cursor  = new Gdk.Cursor (Gdk.CursorType.ARROW);
        add_events (Gdk.EventMask.EXPOSURE_MASK | Gdk.EventMask.BUTTON1_MOTION_MASK | 
        Gdk.EventMask.KEY_PRESS_MASK | Gdk.EventMask.KEY_RELEASE_MASK | 
        Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK |
        Gdk.EventMask.POINTER_MOTION_MASK | Gdk.EventMask.POINTER_MOTION_HINT_MASK);
		size_allocate.connect(on_size_allocate);
		draw.connect(on_draw);
		motion_notify_event.connect(on_motion_notify_event);
    }

    /// Convert logical (mapping) to physical (screen) coordinates
    void log2phys (ref float x, ref float y) {
		Allocation allocation;
        get_allocation(out allocation);
        x = (x - x0) / (x1 - x0) * (allocation.width  - 2) + 1;
        y = (y - y0) / (y1 - y0) * (allocation.height - 2) + 1;
    }

    /// Convert physical (screen) to logical (mapping) coordinates
    void phys2log (ref float x, ref float y) {
		Allocation allocation;		
        get_allocation(out allocation);
        x = x0 + (x - 1) * (x1 - x0) / (allocation.width  - 2);
        y = y0 + (y - 1) * (y1 - y0) / (allocation.height - 2);
    }
    
    
    /// Clip function
    /// @param pt point being clipped
    /// @param x horizontal logical coordinate
    /// @param y vertical logical coordinate
    /// @param hide true if point is outside "valid" range and about to be deleted
    void clip (int pt, ref float x, ref float y, ref bool hide) {
        hide = false;
        clipping (pt, ref x, ref y, ref hide);
        
        float ymin = float.min (y0, y1), ymax = float.max (y0, y1);
        float yamp = ymax - ymin;
        
        if (pt != 0 && pt != (int)(points.length () - 1))
        {
            if (y < ymin - yamp || y > ymax + yamp)
                hide = true;
        }
        
        if (x < x0)   x = x0;
        if (y < ymin) y = ymin;
        if (x > x1)   x = x1;
        if (y > ymax) y = ymax;
        if (pt == 0)  x = 0;
        
        if (pt == (int)(points.length () - 1))
            x = points.nth_data(pt).x;
            
        if (pt > 0 && x < points.nth_data(pt - 1).x)
            x = points.nth_data(pt - 1).x;
            
        if (pt < (int)(points.length () - 1) && x > points.nth_data(pt + 1).x)
            x = points.nth_data(pt + 1).x;
    }


    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context c) {        
        Gdk.RGBA scHot   = { 1.0, 0.0, 0.0, 1.0 };
        Gdk.RGBA scPoint = { 1.0, 1.0, 1.0, 1.0 };
        Gdk.RGBA scLine  = { 0.5, 0.5, 0.5, 1.0 };
        
        if (points.length () > 0)
        {
            Gdk.cairo_set_source_rgba (c, scLine);
            
            for (size_t i = 0; i < points.length (); i++)
            {
                CurvePoint pt = points.nth_data ((uint)i);
                
                if (i == (size_t)cur_pt && hide_current) {
                    continue;
                }
                    
                float x = pt.x, y = pt.y;
                
                log2phys (ref x, ref y);
                
                if (i == 0) {
                    c.move_to (x, y);
                } else {
                    c.line_to (x, y);
                }
            }
            c.stroke ();
        }
        
        for (uint i = 0; i < points.length (); i++)
        {
            if (i == (size_t)cur_pt && hide_current) {
                continue;
            }
                
            CurvePoint pt = points.nth_data(i);
            float x = pt.x, y = pt.y;
            log2phys (ref x, ref y);
            Gdk.cairo_set_source_rgba (c, (i == (size_t)cur_pt) ? scHot : scPoint);
            c.rectangle (x - 2, y - 2, 5, 5);
            c.fill ();
        }

        return true;
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width  = 64;		
        natural_width  = 64;		
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 32;
		natural_height = 32;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 32;
		natural_height = 32;
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = 64;		
        natural_width  = 64;				
	}

    public void on_size_allocate (Gtk.Allocation allocation) {
		set_allocation(allocation);
    
        if (get_realized ()) {
            get_window().move_resize (allocation.x, allocation.y, allocation.width, allocation.height);
        }
    }


    int find_nearest (int ex, int ey, ref int insert_pt) {
        float dist = 5;
        int found_pt = -1;
        for (int i = 0; i < (int)points.length (); i++)
        {
            float x = points.nth_data(i).x;
            float y = points.nth_data(i).y;
            log2phys (ref x, ref y);
            float thisdist = float.max (Math.fabsf (ex - x), Math.fabsf (ey - y));
            
            if (thisdist < dist)
            {
                dist = thisdist;
                found_pt = i;
            }
            
            if (ex > x)
                insert_pt = i + 1;
        }
        return found_pt;
    }

    public override bool button_press_event (Gdk.EventButton event) {
        int found_pt, insert_pt = -1;
        found_pt = find_nearest ((int)event.x, (int)event.y, ref insert_pt);

        if (found_pt == -1 && insert_pt != -1)
        {
            // if at point limit, do not start anything
            if (points.length () >= point_limit) {
                debug ("points.length () >= point_limit: %u >= %u", points.length (), point_limit);
                return true;
            }
                
            float x = (float)event.x;
            float y = (float)event.y;
            bool hide = false;
            phys2log (ref x, ref y);
            points.insert (new CurvePoint (x, y), insert_pt);
            clip (insert_pt, ref x, ref y, ref hide);
            
            if (hide)
            {
                // give up
                points.remove (points.nth_data (insert_pt));
                return true;
            }
            
            points.nth_data(insert_pt).x = x;
            points.nth_data(insert_pt).y = y;
            found_pt = insert_pt;
        }
        
        grab_focus ();
        cur_pt = found_pt;
        queue_draw ();
        
        curve_changed (ref points);
            
        get_window().set_cursor (hand_cursor);
        return true;
    }

    public override bool button_release_event (Gdk.EventButton event) {
        if (cur_pt != -1 && hide_current)
            points.remove (points.nth_data (cur_pt));
        cur_pt = -1;
        hide_current = false;
        
        curve_changed (ref points);
            
        queue_draw ();
        get_window().set_cursor (points.length () >= point_limit ? arrow_cursor : pencil_cursor);
        return false;
    }

    public bool on_motion_notify_event (Gdk.EventMotion event) {
        if (event.is_hint == 1)
        {
            Gdk.Event.request_motions (event);
        }

        if (cur_pt != -1)
        {
            float x = (float)event.x;
            float y = (float)event.y;
            phys2log (ref x, ref y);
            clip (cur_pt, ref x, ref y, ref hide_current);

            points.nth_data(cur_pt).x = x;
            points.nth_data(cur_pt).y = y;

            curve_changed (ref points);
                
            queue_draw ();        
        }
        else
        {
            int insert_pt = -1;
            if (find_nearest ((int)event.x, (int)event.y, ref insert_pt) == -1)
                get_window().set_cursor (points.length () >= point_limit ? arrow_cursor : pencil_cursor);
            else
                get_window().set_cursor (hand_cursor);
        }
        return false;
    }


    public void set_points (ref List<CurvePoint> src)
    {
        if (points.length () != src.length ())
            cur_pt = -1;
        points = (owned)src;
        
        queue_draw ();
    }
}

} // namespace Prolooks
