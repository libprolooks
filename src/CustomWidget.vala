using Gtk;
using Gdk;

// template for new widgets

namespace Prolooks {

public class CustomWidget : DrawingArea {

    public CustomWidget () {

        // Enable the events you wish to get notified about.
        // The 'expose' event is already enabled by the DrawingArea.
        add_events (Gdk.EventMask.BUTTON_PRESS_MASK
                  | Gdk.EventMask.BUTTON_RELEASE_MASK
                  | Gdk.EventMask.POINTER_MOTION_MASK);

		size_allocate.connect(on_size_allocate);
		draw.connect(on_draw);
		motion_notify_event.connect(on_motion_notify_event);
        // Set favored widget size
        set_size_request (100, 100);
    }

    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context c) {   
		Gtk.Allocation allocation;
		get_allocation(out allocation);     

        return false;
    }

    /* Mouse button got pressed over widget */
    public override bool button_press_event (Gdk.EventButton event) {
        // ...
        return false;
    }

    /* Mouse button got released */
    public override bool button_release_event (Gdk.EventButton event) {
        // ...
        return false;
    }

    /* Mouse pointer moved over widget */
    public bool on_motion_notify_event (Gdk.EventMotion event) {
        // ...
        return false;
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width  = 100;		
        natural_width  = minimum_width;		
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = 100;		
        natural_width  = minimum_width;				
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 100;
		natural_height = minimum_height;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 100;
		natural_height = minimum_height;
	}



    public void on_size_allocate (Gtk.Allocation allocation) {
		set_allocation(allocation);
    
        if (get_realized ()) {
            get_window().move_resize (allocation.x, allocation.y, allocation.width, allocation.height);
        }
    }
    
	static int main (string[] args) {
		Gtk.init (ref args);
		var window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
		var widget = new CustomWidget ();
		window.add (widget);
		window.destroy.connect(Gtk.main_quit);
		window.show_all ();
		Gtk.main ();
		return 0;
	}
}

} // namespace Prolooks
