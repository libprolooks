/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {

public abstract class DisplayBase : DrawingArea {
    private Cairo.Surface? cache_surface = null;
    
    public int width {
        get {
            return _width;
        }
    }

    public int height {
        get {
            return _height;
        }
    }
    
    public double inner_x {
        get {
            return _show_glass_rim ? 7.5 : 3.5;
        }
    }

    public double inner_y {
        get {
            return _show_glass_rim ? 6.5 : 3.5;
        }
    }

    private bool _show_glass_rim = true;
    public bool show_glass_rim { 
        get { return _show_glass_rim; } 
        set {
            _show_glass_rim = value;
            cache_surface = null;
            queue_draw ();
        }
    }
    
    public double inner_width {
        get {
            return _show_glass_rim ? (_width - 14) : (_width - 6);
        }
    }

    public double inner_height {
        get {
            return _show_glass_rim ? (_height - 13) : (_height - 6);
        }
    }

    protected int _width  = 383;
    protected int _height = 72;
    
    private bool _show_matrix = true;
    [Description(nick="show matrix", blurb="Whether a pixel matrix should be drawn in the display background (much faster if false)")]
    public bool show_matrix { 
        get { return _show_matrix; } 
        set {
            _show_matrix = value;
            cache_surface = null;
            queue_draw ();
        }
    }
    
    construct {
        add_events (Gdk.EventMask.BUTTON_PRESS_MASK
          | Gdk.EventMask.BUTTON_RELEASE_MASK
          | Gdk.EventMask.POINTER_MOTION_MASK);
		draw.connect(on_draw);
    }
    
    public static void outer_rim_for_display(Cairo.Context cr, double x, double y, double w, double h, RGBA outside, double rounded_corner_radius = 5.0) {
        RGBA inside = RGBA();
        RGBA inside_left = RGBA();
        RGBA inside_top = RGBA();
        RGBA inside_right = RGBA();
        RGBA inside_bottom = RGBA();
         
        inside.parse("#010101");
        inside_left.parse("#525357");
        inside_top.parse("#444444");
        inside_right  = outside;
        inside_bottom = outside;
        
        var horiz_rim_left_x  = rounded_corner_radius - 1;
        var horiz_rim_right_x = w - rounded_corner_radius + 2;
        var top_rim_y         = 1.5;
        var bottom_rim_y      = h - 1.5;
        
        var vert_rim_top_y     = rounded_corner_radius - 1;
        var vert_rim_bottom_y  = h - rounded_corner_radius + 1;
        var right_rim_x        = w - 0.5;
        var left_rim_x         = 1.5;
        
        // inside background
        rounded_rect (cr, 1, 1, w - 2, h - 2, rounded_corner_radius, rounded_corner_radius);
        cairo_set_source_rgba(cr, inside);
        cr.fill ();
        
        // top rim
        cr.move_to (horiz_rim_left_x,  top_rim_y);
        cr.line_to (horiz_rim_right_x, top_rim_y);
        cairo_set_source_rgba(cr, inside_top);
        cr.stroke ();
        
        // right rim
        cr.move_to (right_rim_x, vert_rim_top_y);
        cr.line_to (right_rim_x, vert_rim_bottom_y);
        cairo_set_source_rgba (cr, inside_right);
        cr.stroke ();
        
        // bottom rim
        cr.move_to (horiz_rim_right_x, bottom_rim_y);
        cr.line_to (horiz_rim_left_x,  bottom_rim_y);
        cairo_set_source_rgba (cr, inside_bottom);
        cr.stroke();
        
        // left rim
        cr.move_to (left_rim_x, vert_rim_bottom_y);
        cr.line_to (left_rim_x, vert_rim_top_y);
        cairo_set_source_rgba (cr, inside_left);
        cr.stroke();
    }
    
    public static const string matrix_dot_base_color = "#41403e";
    public static Gdk.RGBA matrix_dot_color () {
        return rgba_shade (rgba_from_string (matrix_dot_base_color), 0.5);
    }
    
    public static void dot_matrix(Cairo.Context cr, double x, double y, double w, double h, Gdk.RGBA color) {
        cr.save();
        cairo_set_source_rgba (cr, color);
        for (double i = x + 1; i < x + w; i++) {
            for (double j = y + 1; j < y + h; j++) {
                if (((long)i % 2) == 0 && ((long)j % 2) == 1) {
                    cr.move_to (i - 0.5, j);
                    cr.line_to (i + 0.5, j);
                    cr.stroke();
                }
            }
        }
        cr.restore();
    }
    
    public static void inner_glass_rim(Cairo.Context cr, double x, double y, double w, double h, double radius = 4.0) {
        RGBA rim_left_top1 = RGBA();
        RGBA rim_left_bottom1 = RGBA();
        RGBA rim_left_top2 = RGBA();
        RGBA rim_left_bottom2 = RGBA();
        RGBA rim_right_top = RGBA();
        RGBA rim_right_bottom = RGBA();
        RGBA rim_top1 = RGBA();
        RGBA rim_top2 = RGBA();
        RGBA rim_bottom = RGBA();
 
        rim_left_top1.parse("#2a282d");
        rim_left_bottom1.parse("#2b2c2e");
        rim_left_top2.parse("#1b191c");
        rim_left_bottom2.parse("#1d181f");
        rim_right_top.parse("#3d3b3c");
        rim_right_bottom.parse("#3a4036");
        rim_top1.parse("#605b5f");
        rim_top2.parse("#2f2e33");
        rim_bottom.parse("#3e403d");
        
        RGBA test = RGBA();
        test.parse("#ff0000");
        
        double ARC_TO_BEZIER = 0.55228475;
        
        if (radius > w - radius)
            radius = w / 2;
        if (radius > h - radius)
            radius = h / 2;

        // approximate (quite close) the arc using a bezier curve
        var c = ARC_TO_BEZIER * radius;

        cr.set_line_width(1.0);

        // rim_top1
        cr.move_to ( x + radius, y);
        cr.rel_line_to ( w - 2 * radius, 0.0);
        cairo_set_source_rgba (cr, rim_top1);
        cr.stroke ();
        
        // rim top2
        cr.move_to ( x + radius, y + 1);
        cr.rel_line_to ( w - 2 * radius, 0.0);
        cairo_set_source_rgba(cr, rim_top2);
        cr.stroke ();

        // right top corner
        cr.move_to (x + w - radius, y);
        cr.rel_curve_to ( c, 0.0, radius, c, radius, radius);
        cairo_set_source_rgba (cr, rim_top1);
        cr.stroke ();
                
        // rim right
        cr.move_to (x + w, y + radius);
        var dy = h - 2 * radius;
        cr.set_source (create_gradient(0, 0, 0, dy, rim_right_top, rim_right_bottom));
        cr.rel_line_to (0, dy);
        cr.stroke ();
        
        // right bottom corner
        cr.move_to (x + w, y + h - radius);
        cr.rel_curve_to ( 0.0, c, c - radius, radius, -radius, radius);
        cairo_set_source_rgba (cr, rim_right_bottom);
        cr.stroke ();
        
        // bottom line
        cr.move_to (x + w - radius, y + h);
        cr.rel_line_to (-w + 2 * radius, 0);
        cairo_set_source_rgba (cr, rim_bottom);
        cr.stroke ();
        
        // bottom left corner
        cr.move_to (x + radius, y + h);
        cr.rel_curve_to ( -c, 0, -radius, -c, -radius, -radius);
        cairo_set_source_rgba (cr, rim_left_bottom1);
        cr.stroke ();
        
        // rim left
        cr.move_to (x, y + h - radius);
        cr.set_source (create_gradient(0, 0, 0, y, rim_left_top1, rim_left_bottom1));
        cr.rel_line_to (0, -h + 2 * radius);
        cr.stroke ();
        
        // rim left shadow
        cr.move_to (x + 1, y + h - radius);
        cr.set_source (create_gradient(0, 0, 0, y, rim_left_top2, rim_left_bottom2));
        cr.rel_line_to (0, -h + 2 * radius);
        cr.stroke ();
        
        // top left corner
        cr.move_to (x, y + radius);
        cr.rel_curve_to (0.0, -c, radius - c, -radius, radius, -radius);
        cairo_set_source_rgba (cr, rim_top1);
        cr.stroke ();

        // top left corner (adding thickness)
        cr.move_to (x + 1, y + radius);
        cr.rel_curve_to (0.0, -c, radius - c, -radius, radius, -radius + 1);
        cairo_set_source_rgba (cr, rim_top1);
        cr.stroke ();
        
    }
    
    public static const string text_color_default = "#9fc717";

    public static void set_font (
        Cairo.Context cr, 
        double size, 
        string           family    = "FreeSans", 
        Cairo.FontSlant  slant     = Cairo.FontSlant.NORMAL, 
        Cairo.FontWeight weight    = Cairo.FontWeight.NORMAL) {
        
        cr.select_font_face (family, slant, weight);
        cr.set_font_size (size);
    }
    
    public static void text (Cairo.Context cr, string text, double x, double y, double size, string color = text_color_default) {
        RGBA text_color = RGBA();
        text_color.parse (color);
        cairo_set_source_rgba (cr, text_color);
        set_font (cr, size);

        cr.move_to (x, y);
        cr.show_text (text);
    }
    
    public static void lcd(Cairo.Context cr, double w, double h, double radius = 5.0) {
        RGBA green_rim_top = RGBA();
        RGBA green_rim_bottom = RGBA();
        green_rim_top.parse ("#2c4329");
        green_rim_bottom.parse ("#101d09");
        cr.set_source (create_gradient (0.0, 0.0, 0.0, h, green_rim_top, green_rim_bottom, 1.0, 1.0));
        rounded_rect (cr, 0.0, 0.0, w, h, radius, radius);
        cr.stroke ();

        cr.set_source (create_gradient (0.0, 0.0, 0.0, h, green_rim_top, green_rim_bottom, 0.5, 0.5));
        rounded_rect (cr, 0.0, 0.0, w, h, radius, radius);
        cr.fill ();
    }
    
    public static void reflection(Cairo.Context cr, double w, double h, double reflection_start = -1) {
        // reflection
		if (reflection_start < 0)
			reflection_start = h - 3;

        cr.new_path ();
        cr.move_to (0.0, 0.0);
        cr.line_to (0.0, reflection_start);
        cr.curve_to (w / 3.0, h / 3.0, w * 2.0 / 3.0, 17.0 * h / 59.0, w, 15.0 * h / 59.0);
        cr.line_to (w, 0.0);
        cr.close_path ();
        cr.set_source_rgba(0.8, 0.9, 0.1, 0.06);
        cr.fill ();
    }
    
    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context c) {        
        //RGBA test_color = RGBA.parse ("#ff0000");
		Gtk.Allocation allocation;
		get_allocation(out allocation);

        if (_width != allocation.width || _height != allocation.height) {
            _width  = allocation.width;
            _height = allocation.height;
            cache_surface = null;
        }
        
        // Create a Cairo context
        var cr = Gdk.cairo_create (this.get_window());
        set_line_width_from_device (cr);


        if (cache_surface == null) {
            cache_surface = new Cairo.Surface.similar (cr.get_target (), 
                                                       Cairo.Content.COLOR_ALPHA,
                                                       _width,
                                                       _height);
                                                       
            Cairo.Context cache_cr = new Cairo.Context ( cache_surface );

            set_line_width_from_device (cache_cr);

            DisplayBase.outer_rim_for_display (cache_cr, 0, 0, _width, _height, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]));
            
            if (show_glass_rim) {
                cache_cr.translate (4.5, 3.5);
                inner_glass_rim (cache_cr, 0, 0, _width - 8, _height - 9, 2);
            }

            cache_cr.translate (3.0, 3.0);
            
            lcd(cache_cr, inner_width, inner_height, _height > 30 ? 5.0 : 3.0);
            
            if (show_matrix) {
                dot_matrix (cache_cr, _show_glass_rim ? 0 : 0.5, _show_glass_rim ? 0 : 0.5, inner_width, inner_height, matrix_dot_color ());
            }
        }
        
        cr.set_source_surface (cache_surface, 0, 0);
        cr.paint ();

        cr.translate (inner_x, inner_y);
                
        draw_contents(cr);
        
        reflection(cr, inner_width, inner_height);
        
        return true;
    }
    
    // Template method
    protected abstract bool draw_contents(Cairo.Context cr);
}

} // namespace Prolooks
