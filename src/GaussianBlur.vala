/* Pixman based blur effect
 * Copyright (C) 2008  Mathias Hasselmann
 * Copyright (C) 2010  Hans Baier
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

namespace Prolooks {

	public static void draw_pixbuf_with_mask (Cairo.Context cr,
											  Gdk.Pixbuf pixbuf,
											  double     x,
											  double     y,
											  double     w,
											  double     h) {
        double dx, dy;

        dx = pixbuf.get_width ();
        dy = pixbuf.get_height ();

        x += Posix.rint ((w - dx) / 2);
        y += Posix.rint ((h - dy) / 2);

        cr.push_group_with_content (Cairo.Content.ALPHA);
        cr.set_source_rgb (1, 1, 1);
		double corner_radius = Math.fmin (w, h) / 3;
        rounded_rect (cr, x, y, dx, dy, corner_radius, corner_radius);
		//cr.rectangle (x, y, dx, dy);
        cr.fill ();
        Cairo.Pattern mask = cr.pop_group ();

        Gdk.cairo_set_source_pixbuf (cr, pixbuf, x, y);
        cr.mask (mask);
	}

	public static void draw_image_surface_with_mask (Cairo.Context cr,
													 Cairo.ImageSurface surface,
													 double     x,
													 double     y,
													 double     w,
													 double     h) {
        double dx, dy;

        dx = surface.get_width ();
        dy = surface.get_height ();

        x += Posix.rint ((w - dx) / 2);
        y += Posix.rint ((h - dy) / 2);

        cr.push_group_with_content (Cairo.Content.ALPHA);
        cr.set_source_rgb (1, 1, 1);
		double corner_radius = Math.fmin (w, h) / 3;
        rounded_rect (cr, x, y, dx, dy, corner_radius, corner_radius);
		//cr.rectangle (x, y, dx, dy);
        cr.fill ();
        Cairo.Pattern mask = cr.pop_group ();

        cr.set_source_surface (surface, x, y);
        cr.mask (mask);
	}


/* G(x,y) = 1/(2 * PI * sigma^2) * exp(-(x^2 + y^2)/(2 * sigma^2))
 */
	private static Pixman.Fixed[] create_gaussian_blur_kernel (int     radius,
															   double  sigma,
															   int    *length) {
        double scale2 = 2.0 * sigma * sigma;
        double scale1 = 1.0 / (Math.PI * scale2);

        Pixman.int size = 2 * radius + 1;
        Pixman.int n_params = size * size;

        Pixman.Fixed[] params = new Pixman.Fixed[n_params + 2];
        double[] tmp = new double[n_params];
		double sum;
        int x, y, i;

        /* caluclate gaussian kernel in floating point format */
        for (i = 0, sum = 0, x = -radius; x <= radius; ++x) {
			for (y = -radius; y <= radius; ++y, ++i) {
				double u = x * x;
				double v = y * y;
				
				tmp[i] = scale1 * Math.exp ( -(u + v) / scale2 );
				
				sum += tmp[i];
			}
        }

        /* normalize gaussian kernel and convert to fixed point format */
        params[0] = size.to_fixed ();
        params[1] = size.to_fixed ();

        for (i = 0; i < n_params; ++i) {
			Pixman.double quot = (tmp[i] / sum);
			params[2 + i] = quot.to_fixed ();
		}

        if (length != null)
			*length = n_params + 2;

        return params;
	}

	public static Cairo.ImageSurface blur_image_surface (Cairo.ImageSurface surface,
														 int                radius,
														 double             sigma) {
        int n_params = 0;

        int width  = surface.get_width  ();
        int height = surface.get_height ();
        int stride = surface.get_stride ();

        /* create pixman image for cairo image surface */
        weak uchar[] surface_data = surface.get_data ();
        Pixman.Image src = new Pixman.Image.for_bits (Pixman.Format.a8, width, height, surface_data, stride);

        /* attach gaussian kernel to pixman image */
        Pixman.Fixed[] params = create_gaussian_blur_kernel (radius, sigma, &n_params);
        src.set_filter (Pixman.Filter.CONVOLUTION, params, n_params);

        /* render blurred image to new pixman image */
        uchar[] result_data = new uchar[stride * height];
        Pixman.Image dst = new Pixman.Image.for_bits (Pixman.Format.a8, width, height, result_data, stride);
        Pixman.image_composite (Pixman.Op.SRC, src, null, dst, 0, 0, 0, 0, 0, 0, (uint16) width, (uint16) height);

        /* create new cairo image for blured pixman image */
        Cairo.ImageSurface result = new Cairo.ImageSurface.for_data (result_data, Cairo.Format.A8, width, height, stride);

		// FIXME: possible memory leak: freeing result_data;
        return result;
	}

	public static Cairo.ImageSurface create_shadow_surface_for_pixbuf (Gdk.Pixbuf pixbuf,
																	   double     w,
																	   double     h,
																	   double     r = 3,
																	   double     alpha = 0.8,
																	   double     sigma = 3.0,
																	   double     offset = 2.0) {

        /* create cairo context for image surface */
        Cairo.ImageSurface surface = new Cairo.ImageSurface (Cairo.Format.A8, (int) (w + r + r), (int) (h + r + r));
        Cairo.Context cr =  new Cairo.Context (surface);

        /* pixbuf to mask pattern */
        cr.push_group_with_content (Cairo.Content.ALPHA);
        draw_pixbuf_with_mask (cr, pixbuf, r, r, w, h);
        Cairo.Pattern pattern = cr.pop_group ();

        /* fill image surface, considering mask pattern from pixbuf */
        cr.set_source_rgba (1, 1, 1, alpha);
        cr.mask (pattern);

        /* render blurred image surface */
        Cairo.ImageSurface blurred_surface = blur_image_surface (surface, (int) r, sigma);

        return blurred_surface;
	}

	public static Cairo.ImageSurface create_shadow_surface (Cairo.ImageSurface orig,
															double     w,
															double     h,
															double     r = 3,
															double     alpha = 0.8,
															double     sigma = 3.0,
															double     offset = 2.0) {

        /* create cairo context for image surface */
        Cairo.ImageSurface surface = new Cairo.ImageSurface (Cairo.Format.A8, (int) (w + r + r), (int) (h + r + r));
        Cairo.Context cr =  new Cairo.Context (surface);

        /* surface to mask pattern */
        cr.push_group_with_content (Cairo.Content.ALPHA);
        draw_image_surface_with_mask (cr, orig, r, r, w, h);
        Cairo.Pattern pattern = cr.pop_group ();

        /* fill image surface, considering mask pattern from orig */
        cr.set_source_rgba (1, 1, 1, alpha);
        cr.mask (pattern);

        /* render blurred image surface */
        Cairo.ImageSurface blurred_surface = blur_image_surface (surface, (int) r, sigma);

        return blurred_surface;
	}


	public static void draw_pixbuf_with_shadow (Cairo.Context cr,
												Gdk.Pixbuf pixbuf,
												double     x,
												double     y,
												double     w,
												double     h,
												double     offset = 2,
												double     radius = 3) {
        /* draw the shadow */
        Cairo.Surface shadow = create_shadow_surface_for_pixbuf (pixbuf, w, h, radius);

        cr.mask_surface (shadow, x + offset/2 - radius, y + offset/2 - radius);

        /* draw the pixbuf */
        draw_pixbuf_with_mask (cr, pixbuf, x - offset/2, y - offset/2, w, h);
	}

	public static void draw_image_surface_with_shadow (Cairo.Context      cr,
													   Cairo.ImageSurface surface,
													   double     x,
													   double     y,
													   double     w,
													   double     h,
													   double     offset = 2,
													   double     radius = 3) {
        /* draw the shadow */
        Cairo.Surface shadow = create_shadow_surface (surface, w, h, radius);

        cr.mask_surface (shadow, x + offset/2 - radius, y + offset/2 - radius);

        /* draw the pixbuf */
        draw_image_surface_with_mask (cr, surface, x - offset/2, y - offset/2, w, h);
	}
} // namespace Prolooks