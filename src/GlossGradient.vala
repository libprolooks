using Gtk;
using Gdk;

// code courtesy http://cocoawithlove.com/2008/09/drawing-gloss-gradients-in-coregraphics.html
// adapted to Vala by Hans Baier

namespace Prolooks {

    private static double perceptualGlossFractionForColor(RGBA color) {
        double REFLECTION_SCALE_NUMBER = 0.2;
        double NTSC_RED_FRACTION       = 0.299;
        double NTSC_GREEN_FRACTION     = 0.587;
        double NTSC_BLUE_FRACTION      = 0.114;

        double glossScale =
            NTSC_RED_FRACTION   * color.red   +
            NTSC_GREEN_FRACTION * color.green +
            NTSC_BLUE_FRACTION  * color.blue;
            
        glossScale = Math.pow(glossScale, REFLECTION_SCALE_NUMBER);
        
        return glossScale;
    }
    
    private static RGBA perceptualCausticColorForColor(RGBA input) {
        double CAUSTIC_FRACTION             = 0.60;
        double COSINE_ANGLE_SCALE           = 1.4;
        double MIN_RED_THRESHOLD            = 0.95;
        double MAX_BLUE_THRESHOLD           = 0.7;
        double GRAYSCALE_CAUSTIC_SATURATION = 0.2;
        
        RGBA source = input;
        
        HSV source_hsv = new HSV();
        source_hsv.from_rgba(source);

        double hue        = source_hsv.hue;
        double saturation = source_hsv.saturation; 
        double value      = source_hsv.value; 
        double alpha      = input.alpha;
        
        HSV target_hsv = new HSV();
        target_hsv.from_rgba(rgba_new(1.0, 1.0, 0.0));
                
        double targetHue        = target_hsv.hue; 
        double targetSaturation = target_hsv.saturation; 
        double targetValue      = target_hsv.value;

        if (saturation < 0.0001) {
            hue = targetHue;
            saturation = GRAYSCALE_CAUSTIC_SATURATION;
        }

        if (hue > MIN_RED_THRESHOLD) {
            hue -= 1.0;
        } else if (hue > MAX_BLUE_THRESHOLD) {
            target_hsv.from_rgba(rgba_new(1.0, 0.0, 1.0));
                
            targetHue        = target_hsv.hue; 
            targetSaturation = target_hsv.saturation; 
            targetValue      = target_hsv.value;
        }

        double scaledCaustic = CAUSTIC_FRACTION * 0.5 * (1.0 + Math.cos(COSINE_ANGLE_SCALE * Math.PI * (hue - targetHue) / 360.0));        
        
        target_hsv.hue        = hue * (1.0 - scaledCaustic) + targetHue * scaledCaustic;
        target_hsv.saturation = saturation;
        target_hsv.value      = value * (1.0 - scaledCaustic) + targetValue * scaledCaustic;

        RGBA target_color = target_hsv.to_rgba();
        target_color.alpha = alpha;

        return target_color;
    }

    private struct GlossParameters {
      RGBA color;
      RGBA caustic;
      double expCoefficient;
      double expScale;
      double expOffset;
      double initialWhite;
      double finalWhite;
    }

    private static RGBA glossInterpolation(double progress, ref GlossParameters params) {
      RGBA output = RGBA();

      if (progress < 0.5)
        {
          progress = progress * 2.0;

          progress =
		  1.0 - params.expScale * (Math.exp(progress * (-params.expCoefficient)) - params.expOffset);

          double currentWhite = progress * (params.finalWhite - params.initialWhite) + params.initialWhite;
        
          output.red   = params.color.red   * (1.0 - currentWhite) + currentWhite;
          output.green = params.color.green * (1.0 - currentWhite) + currentWhite;
          output.blue  = params.color.blue  * (1.0 - currentWhite) + currentWhite;
          output.alpha = params.color.alpha * (1.0 - currentWhite) + currentWhite;
        }
      else
        {
          progress = (progress - 0.5) * 2.0;


          progress = params.expScale *
		  (Math.exp((1.0 - progress) * (-params.expCoefficient)) - params.expOffset);

          output.red   = params.color.red   * (1.0 - progress) + params.caustic.red   * progress;
          output.green = params.color.green * (1.0 - progress) + params.caustic.green * progress;
          output.blue  = params.color.blue  * (1.0 - progress) + params.caustic.blue  * progress;
          output.alpha = params.color.alpha * (1.0 - progress) + params.caustic.alpha * progress;
        }
	  
	  return output;
    }

    public static Cairo.Pattern gloss_gradient_pattern(double x0, double y0, double x1, double y1, RGBA color, int no_steps = 100) {
        double EXP_COEFFICIENT = 1.2;
        double REFLECTION_MAX  = 0.60;
        double REFLECTION_MIN  = 0.20;

        GlossParameters params = GlossParameters();

        params.expCoefficient = EXP_COEFFICIENT;
        params.expOffset      = Math.exp(-params.expCoefficient);
        params.expScale       = 1.0 / (1.0 - params.expOffset);

        params.color = color;

        params.caustic      = perceptualCausticColorForColor(params.color);

        double glossScale   = perceptualGlossFractionForColor(params.color);

        params.initialWhite = glossScale * REFLECTION_MAX;
        params.finalWhite   = glossScale * REFLECTION_MIN;
    
        Cairo.Pattern result = new Cairo.Pattern.linear(x0, y0, x1, y1);
        
        for (double offset = 0; offset <= 1.0; offset += 1.0 / (double) no_steps) {
            RGBA stop = glossInterpolation(offset, ref params);
            result.add_color_stop_rgba(offset, stop.red, stop.green, stop.blue, stop.alpha);
        }
        
        return result;
    }

} // namespace Prolooks
