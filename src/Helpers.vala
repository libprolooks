/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {
    public enum ButtonState {
        NORMAL,
        PRESSED;
    }
    
    public enum ButtonType {
        PRESS_BUTTON,
        TOGGLE_BUTTON;
    }

    public static Gdk.RGBA rgba_new (double red, double green, double blue, double alpha = 1.0) {
      Gdk.RGBA color = RGBA();
      color.red = red;
      color.green = green;
      color.blue = blue;
      color.alpha = alpha;
      return color;
    }

    public static Gdk.RGBA rgba_from_string (string s) {
      Gdk.RGBA color = RGBA();
      color.parse(s);
      color.alpha = 1.0;
      return color;
    }

    public static Gdk.RGBA gdk_color_to_rgba (Gdk.Color color) {
      Gdk.RGBA rgba = RGBA();
        rgba.red   = (double)color.red   / (double)uint16.MAX;
        rgba.green = (double)color.green / (double)uint16.MAX;
        rgba.blue  = (double)color.blue  / (double)uint16.MAX;
	return rgba;
    }

    public static void add_color_stop_to (Cairo.Pattern p, double offset, Gdk.RGBA color) {
      p.add_color_stop_rgba (offset, color.red, color.green, color.blue, color.alpha);
    }
    
    public static void set_line_width_from_device (Cairo.Context cr) {
        double ux=1, uy=1;
        cr.device_to_user (ref ux, ref uy);
        if (ux < uy) {
            ux = uy;
        }
        cr.set_line_width (ux);
    }
	
    public static void set_source_color (Cairo.Context cr, Gdk.RGBA color, double alpha = 1.0) {
      cr.set_source_rgba(color.red, color.green, color.blue, alpha);
    }
            
    public static void set_source_color_string (Cairo.Context cr, string color, double alpha = 1.0) {
    	Gdk.RGBA c = rgba_from_string(color);
	c.alpha = alpha;
        Gdk.cairo_set_source_rgba (cr, c);
    }
    
    public static void add_color_stop (Cairo.Pattern p, double offset, Gdk.RGBA color, double alpha = 1.0) {
      p.add_color_stop_rgba (offset, color.red, color.green, color.blue, alpha);
    }

    public static void add_color_stop_str (Cairo.Pattern p, double offset, string color, double alpha = 1.0) {
      add_color_stop (p, offset, rgba_from_string (color), alpha);
    }
    
    public static Cairo.Pattern create_gradient (double x1, double y1, double x2, double y2, Gdk.RGBA start, Gdk.RGBA stop, double alpha_start = 1.0, double alpha_stop = 1.0) {
        Cairo.Pattern gradient = new Cairo.Pattern.linear(x1, y1, x2, y2);
        add_color_stop(gradient, 0, start, alpha_start);
        add_color_stop(gradient, 1, stop,  alpha_stop);
        return gradient;
    }

    public static Cairo.Pattern create_gradient_str (double x1, double y1, double x2, double y2, string start, string stop, double alpha_start = 1.0, double alpha_stop = 1.0) {
        return create_gradient (x1, y1, x2, y2, rgba_from_string (start), rgba_from_string (stop), alpha_start, alpha_stop);
    }
    
    public static void rounded_rect (Cairo.Context cr, double x, double y, double w, double h, double radius_x=5, double radius_y=5) {
        // from mono moonlight aka mono silverlight
        // test limits (without using multiplications)
        // http://graphics.stanford.edu/courses/cs248-98-fall/Final/q1.html
        double ARC_TO_BEZIER = 0.55228475;
        
        if (radius_x > w - radius_x)
            radius_x = w / 2;
        if (radius_y > h - radius_y)
            radius_y = h / 2;

        // approximate (quite close) the arc using a bezier curve
        var c1 = ARC_TO_BEZIER * radius_x;
        var c2 = ARC_TO_BEZIER * radius_y;

        cr.new_path();
        cr.move_to ( x + radius_x, y);
        cr.rel_line_to ( w - 2 * radius_x, 0.0);
        cr.rel_curve_to ( c1, 0.0, radius_x, c2, radius_x, radius_y);
        cr.rel_line_to ( 0, h - 2 * radius_y);
        cr.rel_curve_to ( 0.0, c2, c1 - radius_x, radius_y, -radius_x, radius_y);
        cr.rel_line_to ( -w + 2 * radius_x, 0);
        cr.rel_curve_to ( -c1, 0, -radius_x, -c2, -radius_x, -radius_y);
        cr.rel_line_to (0, -h + 2 * radius_y);
        cr.rel_curve_to (0.0, -c2, radius_x - c1, -radius_y, radius_x, -radius_y);
        cr.close_path ();
    }
    
    public static void background_gradient(Cairo.Context cr, double w, double h) {
        // outside background
        Gdk.RGBA background_gradient_start = RGBA();
        Gdk.RGBA background_gradient_stop = RGBA();
        background_gradient_start.parse("#bebdc2");
        background_gradient_stop.parse("#b1b4b9");
        
        cr.rectangle (0, 0, w, h);
        Cairo.Pattern background_gradient = new Cairo.Pattern.linear(0, 0, 0, h);
        add_color_stop(background_gradient, 0, background_gradient_start);
        add_color_stop(background_gradient, 1, background_gradient_stop);
        cr.set_source (background_gradient);
        cr.fill ();
    }

    public static double modula(double number, double divisor) {
        return (((int)number % (int)divisor) + (number - (int)number));
    }
    
    public class HSL {
        public double hue         {get; set;}
        public double saturation  {get; set;}
        public double lightness   {get; set;}
	public double alpha       {get; set;}
        
	public HSL() {
	  alpha = 1.0;
	}

        public string to_string() {
            return "HSL (%f, %f, %f)".printf (hue, saturation, lightness);
        }

        public Gdk.RGBA to_rgba() {
            int i;
            var hue_shift   = new double[3];
            var color_shift = new double[3];
            double m1;
            double m2;
            double m3;
        
            if (lightness <= 0.5)
                m2 = lightness * (1 + saturation);
            else
                m2 = lightness + saturation - lightness * saturation;
 
            m1 = 2 * lightness - m2;
 
            hue_shift[0] = hue + 120;
            hue_shift[1] = hue;
            hue_shift[2] = hue - 120;
 
            color_shift[0] = color_shift[1] = color_shift[2] = lightness;    
 
            i = (saturation == 0) ? 3 : 0;
 
            for (; i < 3; i++)
            {
                m3 = hue_shift[i];
 
                if (m3 > 360)
                    m3 = modula(m3, 360);
                else if (m3 < 0)
                    m3 = 360 - modula(Math.fabs(m3), 360);
 
                if (m3 < 60)
                    color_shift[i] = m1 + (m2 - m1) * m3 / 60.0;
                else if (m3 < 180)
                    color_shift[i] = m2;
                else if (m3 < 240)
                    color_shift[i] = m1 + (m2 - m1) * (240 - m3) / 60.0;
                else
                    color_shift[i] = m1;
            }    
 
            Gdk.RGBA color = { color_shift[0], color_shift[1], color_shift[2], alpha};
 
            return color;
        }
                
        public void from_rgba(Gdk.RGBA color) {
            double min, max, delta;
            
            double red   = color.red;
            double green = color.green;
            double blue  = color.blue;
	    this.alpha = color.alpha;
            
            if (red > green) {
                if (red > blue)
                  max = red;
                else
                  max = blue;

                if (green < blue)
                  min = green;
                else
                  min = blue;
            } else {
                if (green > blue)
                  max = green;
                else
                  max = blue;

                if (red < blue)
                  min = red;
                else
                  min = blue;
            }
      
            lightness = (max + min) / 2.0;
     
            if (Math.fabs(max - min) < 0.0001) {
                hue = 0.0;
                saturation = 0.0;
            } else {
                if (lightness <= 0.5)
                    saturation = (max - min) / (max + min);
                else
                    saturation = (max - min) / (2.0 - max - min);
       
                delta = max -min;

                if (red == max)
                    hue = (green - blue) / delta;
                else if (green == max)
                    hue = 2.0 + (blue - red) / delta;
                else if (blue == max)
                    hue = 4.0 + (red - green) / delta;

                hue *= 60.0;
                if (hue < 0.0)
                    hue += 360.0;
            }
        }
    }
    
    public class HSV {
        public double hue         {get; set;}
        public double saturation  {get; set;}
        public double value       {get; set;}
	public double alpha       {get; set;}
        
	public HSV() {
	  alpha = 1.0;
	}
	
        public string to_string() {
            return "HSV (%f, %f, %f)".printf (hue, saturation, value);
        }

	public HSV.for_rgba (Gdk.RGBA color) {
	  from_rgba (color);
	}

        public Gdk.RGBA to_rgba() {
	  double r = 0.0, g = 0.0, b = 0.0;
	  double v = value;
	  int hi;
	  double f, p, q, t;
  
	  hi = (int) modula(Math.floor(hue / 60.0), 6);
	  f  = hue / 60.0 - Math.floor(hue / 60.0);
	  p  = value * (1.0 - saturation);
	  q  = value * (1.0 - f * saturation);
	  t  = value * (1.0 - (1.0 - f) * saturation);

	  switch (hi) {
	  case 0: r = value; g = t; b = p; break;
	  case 1: r = q; g = value; b = p; break;
	  case 2: r = p; g = value; b = t; break;
	  case 3: r = p; g = q; b = value; break;
	  case 4: r = t; g = p; b = value; break;
	  case 5: r = value; g = p; b = q; break;
	  default: break;
	  }
 
	  Gdk.RGBA color = {r, g, b, alpha };
 
	  return color;
        }
        
        public void from_rgba(Gdk.RGBA color) {
            double min, max, delta;
            
            double red   = color.red;
            double green = color.green;
            double blue  = color.blue;
	    this.alpha   = color.alpha;
            
            if (red > green) {
                if (red > blue)
                  max = red;
                else
                  max = blue;

                if (green < blue)
                  min = green;
                else
                  min = blue;
            } else {
                if (green > blue)
                  max = green;
                else
                  max = blue;

                if (red < blue)
                  min = red;
                else
                  min = blue;
            }
      
            value = max;
     
            if (Math.fabs(max - min) < 0.0001)
            {
                hue = 0.0;
                saturation = 0.0;
            }    
            else
            {
                if (max < 0.0001)
                    saturation = 0;
                else
                    saturation = (max - min) / max;
       
                delta = max - min;

                if (red == max)
                    hue = (green - blue) / delta;
                else if (green == max)
                    hue = 2.0 + (blue - red) / delta;
                else if (blue == max)
                    hue = 4.0 + (red - green) / delta;

                hue *= 60.0;
                if (hue < 0.0)
                    hue += 360.0;
            }
        }
    }
    
    public static Gdk.RGBA rgba_shade(Gdk.RGBA orig, double shade_ratio) {
      HSL HSL = new HSL ();
      HSL.from_rgba (orig);
        
      HSL.lightness = Math.fmin (HSL.lightness * shade_ratio, 1.0);
      HSL.lightness = Math.fmax (HSL.lightness, 0.0);
      
      HSL.saturation = Math.fmin (HSL.saturation * shade_ratio, 1.0);
      HSL.saturation = Math.fmax (HSL.saturation, 0.0);

      Gdk.RGBA result = HSL.to_rgba ();
      return result;
    }
	
	public static Gdk.Pixbuf cairo_image_surface_to_pixbuf (Cairo.ImageSurface surface) {
		if (surface.get_format() != Cairo.Format.ARGB32)
			return null;

		weak uchar[] knob_data = surface.get_data ();
		
		for (int i = 0; i < surface.get_height() * surface.get_stride(); i += 4) {
			uchar r = knob_data[i+0];
			uchar g = knob_data[i+1];
			uchar b = knob_data[i+2];
			uchar a = knob_data[i+3];

			//FIXME: Undo premultiplied alpha here
			knob_data[i+0] = b;
			knob_data[i+1] = g;
			knob_data[i+2] = r;
			knob_data[i+3] = a;
		}
			
		return new Gdk.Pixbuf.from_data (knob_data, Gdk.Colorspace.RGB, true, 8, surface.get_width(), surface.get_height(), surface.get_stride() , null);
	}

} // namespace Prolooks
