/* 
    Copyright 2010 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    public interface IKnobImageSource : GLib.Object {        
        public int variants {
            get {
                return (int)KnobMode.ENDLESS + 1;
            }
        }
        
        public int phases {
            get {
                return 65;
            }
        }
        
        public abstract double get_knob_width ();

        public abstract double get_knob_height ();

        public abstract double get_line_width ();

        public abstract double get_radius ();

        public abstract string to_string ();

        public virtual void paint_knobs (Cairo.Context cr, KnobMode knob_mode, double first_x, double first_y) {
            var x = first_x;
            var y = first_y;
            for (var phase = 0; phase < phases; phase++) {
                paint_knob (cr, knob_mode, phase, get_line_width (), get_radius (), x, y);
                x += get_knob_width ();
            }
        }
        
        public abstract void paint_knob (Cairo.Context cr, KnobMode knob_mode, int phase,  double lwidth, double radius, double x, double y);
    }

} // namespace Prolooks
