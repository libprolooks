/* 
    Copyright 2009 by Hans Baier, Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

/// Structure with information needed for drawing a single key
public struct KeyInfo {
    public double x; ///< X coordinate of the top-left point of the key
    public double y; ///< Y coordinate of the top-left point of the key
    public double width; ///< key width
    public double height; ///< key height
    public int note; ///< MIDI note number
    public bool black; ///< true if it's a black key, false if it's a white key
}

public class Keyboard : DrawingArea {
    /// called before drawing key interior
    /// @retval true do not draw the key
    public signal bool pre_draw (Cairo.Context c, ref KeyInfo ki);

    /// @retval true do not draw the outline
    /// called before drawing key outline of white keys
    public signal bool pre_draw_outline (Cairo.Context c, ref KeyInfo ki);

    /// called after key is drawn using standard method (but not if drawing is skipped)
    public signal void post_draw (Cairo.Context c, ref KeyInfo ki);

    /// called after key is drawn
    public signal void post_all (Cairo.Context c);

    /// key was pressed
    public signal void note_on (int note, int vel);

    /// key was released
    public signal void note_off (int note);

    /// Range (number of white keys = number of octaves * 7 + 1)
    int nkeys;
        
    /// The note currently pressed via mouse selection
    int last_key;
    
    /// If true, the keyboard accepts mouse clicks and keys
    [Description(blurb="whether the keyboard generates note_on / note_off signals when its keys are pressed")]
    public bool interactive { get; set; }

    static const int[] semitones_b = { 1, 3, -1, 6, 8, 10, -1 };
    static const int[] semitones_w = { 0, 2, 4, 5, 7, 9, 11 };


    construct {
        set_can_focus(true);
        nkeys = 7 * 3 + 1;
        last_key = -1;
        add_events (Gdk.EventMask.EXPOSURE_MASK | Gdk.EventMask.BUTTON1_MOTION_MASK | 
        Gdk.EventMask.KEY_PRESS_MASK | Gdk.EventMask.KEY_RELEASE_MASK | 
        Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK);
		size_allocate.connect(on_size_allocate);
		draw.connect(on_draw);
		set_size_request(12 * nkeys + 1, 32);
    }

    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context c) {        
		Gtk.Allocation allocation;
		get_allocation(out allocation);

        Gdk.RGBA scWhiteKey = { 1, 1, 1, 1 };
        Gdk.RGBA scBlackKey = { 0, 0, 0, 1 };
        Gdk.RGBA scOutline =  { 0, 0, 0, 1 };

        int sy = allocation.height - 1;
        c.set_line_join (Cairo.LineJoin.MITER);
        c.set_line_width (1);
        
        for (int i = 0; i < nkeys; i++)
        {
            KeyInfo ki = { 0.5 + 12 * i, 0.5, 12, sy, 12 * (i / 7) + semitones_w[i % 7], false };
            c.new_path ();
            Gdk.cairo_set_source_rgba (c, scWhiteKey);
            if (!pre_draw (c, ref ki))
            {
                c.rectangle (ki.x, ki.y, ki.width, ki.height);
                c.fill_preserve ();
                Gdk.cairo_set_source_rgba (c, scOutline);
                if (!pre_draw_outline (c, ref ki))
                    c.stroke ();
                else
                    c.new_path ();
                post_draw (c, ref ki);
            }
        }

        for (int i = 0; i < nkeys - 1; i++)
        {
            if (((1 << (i % 7)) & 59) != 0)
            {
                KeyInfo ki = { 8.5 + 12 * i, 0.5, 8, sy * 3 / 5, 12 * (i / 7) + semitones_b[i % 7], true };
                c.new_path ();
                c.rectangle (ki.x, ki.y, ki.width, ki.height);
                Gdk.cairo_set_source_rgba (c, scBlackKey);
                if (!pre_draw (c, ref ki))
                {
                    c.fill ();
                    post_draw (c, ref ki);
                }
            }
        }
        
        post_all (c);

        return true;
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width  = 12 * nkeys + 1;		
        natural_width  = 12 * nkeys + 1;		
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 32;
		natural_height = 32;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 32;
		natural_height = 32;
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = 12 * nkeys + 1;		
        natural_width  = 12 * nkeys + 1;				
	}

    public void on_size_allocate (Gtk.Allocation allocation) {
		set_allocation(allocation);
    
        if (get_realized ()) {
            get_window().move_resize (allocation.x + (allocation.width - allocation.width) / 2, 
									  allocation.y, 
									  allocation.width, 
									  allocation.height );
        }
    }

    public override bool key_press_event (Gdk.EventKey event) {
        return false;
    }

    int pos_to_note (int x, int y, out int vel = null) {
		Gtk.Allocation allocation;
			get_parent().get_allocation(out allocation);
        // first try black keys
        if (y <= allocation.height * 3 / 5 && x >= 0 && (x - 8) % 12 < 8) {
            int blackkey = (x - 8) / 12;
            if (blackkey < nkeys && ((59 & (1 << (blackkey % 7))) != 0)) {
                return semitones_b[blackkey % 7] + 12 * (blackkey / 7);
            }
        }
        // if not a black key, then which white one?
        int whitekey = x / 12;
        
        // semitones within octave + 12 semitones per octave
        return semitones_w[whitekey % 7] + 12 * (whitekey / 7);
    }

    public override bool button_press_event (Gdk.EventButton event) {
        if (!interactive)
            return false;
        grab_focus ();
        int vel = 127;
        last_key = pos_to_note ((int)event.x, (int)event.y, out vel);
        if (last_key != -1)
            note_on (last_key, vel);
        return false;
    }

    public override bool button_release_event (Gdk.EventButton event) {
        if (!interactive)
            return false;
        if (last_key != -1)
            note_off (last_key);
        return false;
    }

    public override bool motion_notify_event (Gdk.EventMotion event) {
        if (!interactive)
            return false;
        int vel = 127;
        int key = pos_to_note ((int)event.x, (int)event.y, out vel);
        if (key != last_key)
        {
            if (last_key != -1)
                note_off (last_key);
            last_key = key;
            if (last_key != -1)
                note_on (last_key, vel);
        }
        return false;
    }
    
} // class Keyboard

} // namespace Prolooks
