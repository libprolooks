/* 
    Copyright 2009 by Hans Baier, Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

    static const uint16 GDK_Home    = 0xff50;
    static const uint16 GDK_End     = 0xff57;
    static const uint16 GDK_Up      = 0xff52;
    static const uint16 GDK_Down    = 0xff54;
    static const uint16 GDK_Shift_L = 0xffe1;
    static const uint16 GDK_Shift_R = 0xffe2;

    public enum KnobMode { 
        POSITIVE,
        BIPOLAR,
        NEGATIVE,
        ENDLESS
    }

public class Knob : Range {
    private static HashTable<string, Cairo.Surface> images = null;


    private Cairo.Surface? cache_surface = null;
	
	public void* user_data { get; set; }
    private IKnobImageSource _image_source;
    [Description(nick="image source", blurb="provides the routines for drawing the knob images")]
    public  IKnobImageSource image_source { 
        get {
            if (_image_source == null) {
                var isource = new SimpleKnobImageSource ();
                isource.led_color = rgba_from_string ("#9fc717");
                _image_source = isource;
                set_size_request((int)image_source.get_knob_width (), (int)image_source.get_knob_height ());
            }
            return _image_source;
        } 
        
        set {
            if (value != null && value != _image_source) {
                _image_source = value;
                cache_surface = null;
                set_size_request((int)image_source.get_knob_width (), (int)image_source.get_knob_height ());
                queue_draw ();
            }
        }
    }

    private KnobMode _knob_mode;
    [Description(nick="knob mode", blurb="determines, how the knob will work / look like")]
    public KnobMode knob_mode { 
        get {
            return _knob_mode;
        }
        set {
            if (value != _knob_mode) {
                _knob_mode = value;
                cache_surface = null;
                queue_draw ();
            }
        }
    }
    
    [Description(nick="decimal places", blurb="The value will be rounded to the given number of decimal places")]
    public uint decimal_places { get; set; default = 2; }
    
    double start_value;
    int    start_x;
    int    start_y;
    int    last_y;

    static construct {
        images = new HashTable<string, Cairo.Surface> (GLib.str_hash, GLib.str_equal);
    }

    construct {
        Adjustment adj = new Adjustment(0, 0, 1, 0.01, 0.5, 0);
        set_adjustment (adj);
        set_can_focus(true);
        add_events (Gdk.EventMask.KEY_PRESS_MASK | Gdk.EventMask.KEY_RELEASE_MASK );
		draw.connect(on_draw);
		motion_notify_event.connect(on_motion_notify_event);
    }
    
    public Knob.with_adjustment (Adjustment an_adjustment) {
        set_adjustment (an_adjustment);
    }

    Cairo.Surface create_cache_surface_if_necessary (Cairo.Surface target) {
        string image_state = "%s;knob_mode=%u".printf (image_source.to_string (), knob_mode);

        debug ("looking up state: '%s', hash: '%u'\n", image_state, GLib.str_hash(image_state));
        Cairo.Surface? cached_surface = images.lookup (image_state);
        debug ("Result: %u\n", (uint)cached_surface);
        if ( cached_surface != null) {
            debug ("Cache hit!\n");
            return cached_surface;
        }

        debug ("Cache miss!\n");

        // looks like its either first call or the widget has been resized.
        // create the cache_surface.
        var cache_width  = image_source.get_knob_width () * image_source.phases;
        var cache_height = image_source.get_knob_height ();

        Cairo.Surface surface = new Cairo.Surface.similar (target, 
                                                           Cairo.Content.COLOR_ALPHA,
                                                           (int)cache_width,
                                                           (int)cache_height);
        
        Cairo.Context cache_cr = new Cairo.Context ( surface );
        cache_cr.set_source_rgba (0, 0, 0, 0);
        cache_cr.rectangle (0, 0, cache_width, cache_height);
        cache_cr.fill ();
        
        image_source.paint_knobs (cache_cr, 
                                  knob_mode, 
                                  image_source.get_knob_width () / 2,
                                  image_source.get_knob_height () / 2);

        images.insert (image_state.dup(), surface);
        debug ("HashTable size: %u\n", images.size());
        images.foreach ((k, v) => { debug ("Key: '%s', Value: %x\n", (string)k, (uint)v); });
        return surface;
    }

	public bool on_draw (Cairo.Context cr) {        
		Gtk.Allocation allocation;
		get_allocation(out allocation);
        Adjustment adj = get_adjustment ();

        debug ("adjustment = %p value = %f\n", adj, adj.value);
        int ox = allocation.x, oy = allocation.y;
                
        int phase = (int)((adj.value - adj.lower) * 64 / (adj.upper - adj.lower));
        // skip middle phase except for true middle value
        if (knob_mode == KnobMode.BIPOLAR && phase == 32) {
            double pt = (adj.value - adj.lower) * 2.0 / (adj.upper - adj.lower) - 1.0;

            if (pt < 0)
                phase = 31;

            if (pt > 0)
                phase = 33;
        }
        
        // endless knob: skip 90deg highlights unless the value is really a multiple of 90deg
        if (knob_mode == KnobMode.ENDLESS && (phase % 16) == 0) {
            if (phase == 64)
                phase = 0;

            double nom  = adj.lower + phase * (adj.upper - adj.lower) / 64.0;
            double diff = (adj.value - nom) / (adj.upper - adj.lower);

            if (diff > 0.0001)
                phase = (phase + 1) % 64;

            if (diff < -0.0001)
                phase = (phase + 63) % 64;
        }
        
        if (cache_surface == null) {
            cache_surface = create_cache_surface_if_necessary (cr.get_target ());
        }

        assert (cache_surface != null);
        cr.set_source_surface (cache_surface, - phase * image_source.get_knob_width (), oy);
        cr.rectangle (0, 0, image_source.get_knob_width (), image_source.get_knob_height ());
        cr.fill ();
        
        debug ("exposed %p %d+%d", get_window(), allocation.x, allocation.y);
        if (is_focus) {
			get_style_context().render_focus (
				cr,	
				0, 0, 
				(int)image_source.get_knob_width (), (int)image_source.get_knob_height ());
        }        
        
        return true;
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width = (int)image_source.get_knob_width ();
        natural_width = (int)image_source.get_knob_width ();
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = (int)image_source.get_knob_height ();
		natural_height = (int)image_source.get_knob_height ();
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = (int)image_source.get_knob_height ();
		natural_height = (int)image_source.get_knob_height ();
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width = (int)image_source.get_knob_width ();
        natural_width = (int)image_source.get_knob_width ();
	}

     public new double get_value () {
         debug ("Value: %1.30f\n", base.get_value());
         var rounded = GLib.Math.rint (base.get_value() * Math.pow (10.0, decimal_places))
                    / Math.pow (10.0, decimal_places);
         debug ("Rounded Value: %1.30f\n", rounded);
         return rounded;
     }

    void knob_incr (int dir_down) {
        Adjustment adj = get_adjustment ();
        
        int oldstep = (int)(0.5 + (adj.value - adj.lower) / adj.step_increment);
        int step;
        int nsteps = (int)(0.5 + (adj.upper - adj.lower) / adj.step_increment); // less 1 actually
        if (dir_down != 0)
            step = oldstep - 1;
        else
            step = oldstep + 1;
        if (knob_mode == KnobMode.ENDLESS && step >= nsteps)
            step %= nsteps;
        if (knob_mode == KnobMode.ENDLESS && step < 0)
            step = nsteps - (nsteps - step) % nsteps;
        
        // trying to reduce error cumulation here, by counting from lowest or from highest
        double value = (adj.lower + step * (adj.upper - adj.lower) / nsteps);
        set_value (value);
        // debug ("step %d:%d nsteps %d value %f:%f\n", oldstep, step, nsteps, oldvalue, value);
    }


    public override bool key_press_event (Gdk.EventKey event) {
        Adjustment adj = get_adjustment ();

        switch (event.keyval)
        {
            case GDK_Home:
                set_value (adj.lower);
                return true;

            case GDK_End:
                set_value (adj.upper);
                return true;

            case GDK_Up:
                knob_incr (0);
                return true;

            case GDK_Down:
                knob_incr (1);
                return true;
                
            case GDK_Shift_L:
            case GDK_Shift_R:
                start_value = (int)get_value ();
                start_y = last_y;
                return true;
        }
        
        return false;
    }

    private Gdk.Cursor? cursor = null;

    public override bool key_release_event (Gdk.EventKey event) {
        if (event.keyval == GDK_Shift_L || event.keyval == GDK_Shift_R) {
            start_value = (int)get_value ();
            start_y = last_y;
            return true;
        }
        
        return false;
    }

    public override bool button_press_event (Gdk.EventButton event)
    {
        grab_focus ();
        grab_add (this);
        start_x = (int)event.x;
        start_y = (int)event.y;
        start_value = get_value ();
        debug ("PRESS: start_value: %f, event.y: %d, start_y: %d", start_value, (int)event.y, (int)start_y);

        cursor = get_window().get_cursor ();
        get_window().set_cursor (new Gdk.Cursor (Gdk.CursorType.SB_V_DOUBLE_ARROW));        
        return true;
    }

    public override bool button_release_event (Gdk.EventButton event)
    {
        if (has_grab ()) {
            grab_remove (this);

            if (cursor != null)
	            get_window().set_cursor (cursor);
			else
                get_window().set_cursor (new Gdk.Cursor (Gdk.CursorType.ARROW));
        }
        return false;
    }

    inline double endless (double value)
    {
        if (value >= 0.0)
            return Math.fmod (value, 1.0);
        else
            return Math.fmod (1.0 - Math.fmod (1.0 - value, 1.0), 1.0);
    }

    inline double deadzone (double value, double incr, double scale)
    {
        double dzw = 10.0 / scale;
        if (value >= 0.501)
            value += dzw;
        if (value < 0.499)
            value -= dzw;
        
        value += incr;
        
        if (value >= (0.5 - dzw) && value <= (0.5 + dzw))
            return 0.5;
        if (value < 0.5)
            return value + dzw;
        return value - dzw;
    }

    public bool on_motion_notify_event (Gdk.EventMotion event) {
        Adjustment adj = get_adjustment ();
        double scale = (event.state == Gdk.ModifierType.SHIFT_MASK) ? 1000.0 : 100.0;
        scale /= (adj.upper - adj.lower);
        bool moved = false;
       
        if (has_grab ()) 
        {
            //debug ("start_value: %d, event.y: %d, start_y: %d", (int)start_value, (int)event.y, (int)start_y);
            if (knob_mode == KnobMode.ENDLESS) {
                set_value (endless ((start_value - (event.y - start_y) / scale)));
            } else if (knob_mode == KnobMode.BIPOLAR) {
                set_value (deadzone (start_value, (-(event.y - start_y) / scale), scale));
            } else {
                set_value (start_value - (event.y - start_y) / scale);
            }
            moved = true;
        }
        last_y = (int)event.y;
        return moved;
    }

    public override bool scroll_event (Gdk.EventScroll event) {
        knob_incr (event.direction);
        return true;
    }

} // class Knob

} // namespace Prolooks
