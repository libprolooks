/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

public class KnobWithDisplay : EventBox {
    private Box vbox;
    private Knob knob;
    private ValueDisplay display;

    public string unit { get; set; }

    // This function is called whenever the knob value changes
    public delegate void DisplayFunc (Knob knob, ValueDisplay display, KnobWithDisplay knob_with_display);
    
    private DisplayFunc? display_func = null;
    public void set_display_func (DisplayFunc func) {
        display_func = func;
    }

    // delegating properties
    [Description(nick="show matrix", blurb="Whether a pixel matrix should be drawn in the display background (much faster if false)")]
    public bool show_matrix       { get { return display.show_matrix;    } set { display.show_matrix = value; } }
    public Adjustment adjustment  { get { return knob.get_adjustment (); } set { knob.set_adjustment (value); } }
    [Description(nick="knob mode", blurb="determines, how the knob will work / look like")]
    public KnobMode knob_mode     { get { return knob.knob_mode;         } set { knob.knob_mode = value;      } }
    [Description(nick="decimal places", blurb="The value will be rounded to the given number of decimal places")]
    public uint  decimal_places   { get { return knob.decimal_places;    } set { knob.decimal_places = value; } }
    
    construct {
        vbox = new Box (Gtk.Orientation.VERTICAL, 0);
        knob = new Knob ();
        vbox.pack_start (knob);
        display = new ValueDisplay ();
        vbox.pack_start (display);
        knob.user_data = display;
		decimal_places = 2;
        var adj = knob.get_adjustment ();
        knob.value_changed += (knob) => {
            ValueDisplay disp = knob.user_data as ValueDisplay;
            string text;
            
            if (display_func != null) {
                display_func (knob, display, this);
            } else {
                text = "%.*f".printf (knob.decimal_places, knob.get_value ()) + " " + unit;
                disp.text = text;
            }
        };
        
        add (vbox);
        show_matrix = true;
        GLib.Timeout.add (512, ()=> {
            knob.value_changed ();
        });
    }
}

} // namespace Prolooks

