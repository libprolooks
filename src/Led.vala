/* 
    Copyright 2009 by Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

public class Led : DrawingArea {

    private bool _led_state = false;
    
    Gdk.RGBA base_color = Gdk.RGBA();

    public void set_rgb(double _r, double _g, double _b)
    {
        base_color.red = _r;
        base_color.green = _g;
        base_color.blue = _b;
	base_color.alpha = 1.0;
        queue_draw();
    }

    public void get_rgb(ref double _r, ref double _g, ref double _b)
    {
        _r = base_color.red;
        _g = base_color.green;
        _b = base_color.blue;
    }

    
    [Description(nick="LED is on", blurb="Whether the LED is on or off")]
    public bool led_state {
        set {
            _led_state = value;
            queue_draw();
        }
        get {
            return _led_state;
        }
    }
    
    public Led () {
		// Set favored widget size
        set_size_request (20, 20);
	draw.connect(on_draw);
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
	  minimum_width  = 20;		
	  natural_width  = 20;		
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 20;
		natural_height = 20;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 20;
		natural_height = 20;
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = 20;		
        natural_width  = 20;				
	}
    
    [Description(nick="LED color", blurb="color as a string as accepted by  gdk_rgba_parse (), eg: #a0b1c2")]
    public string color {
        set {
            if (value != null & value != "") {
	        base_color.parse(value);
		base_color.alpha = 1.0;
            } else {
                base_color.parse("rgb(1,0,0)");
		base_color.alpha = 1.0;
            }
        }
    }

	public static void paint_led (Cairo.Context cr, Gdk.RGBA led_color, int width, int height, bool led_state) {
		HSV hsv = new HSV();
		hsv.from_rgba (led_color);
		if (led_state) {
			hsv.saturation = 0.76;
			hsv.value      = 0.87;
		} else {
			hsv.saturation = 0.74;
			hsv.value      = 0.78;
		}
		Gdk.RGBA base_color = hsv.to_rgba ();

		if (led_state) {
			hsv.saturation = 0.89;
			hsv.value      = 0.93;
		} else {
			hsv.saturation = 0.97;
			hsv.value      = 0.76;
		}
		Gdk.RGBA border_color = hsv.to_rgba ();

		if (led_state) {
			hsv.saturation = 0.38;
			hsv.value      = 0.98;
		} else {
			hsv.saturation = 0.33;
			hsv.value      = 0.88;
		}
		Gdk.RGBA bottom_gloss_color = hsv.to_rgba ();
		bottom_gloss_color.alpha = 0.7;

		if (led_state) {
			hsv.saturation = 0.62;
			hsv.value      = 0.89;
		} else {
			hsv.saturation = 0.60;
			hsv.value      = 0.96;
		}
		Gdk.RGBA bottom_gloss_color2 = hsv.to_rgba();
		bottom_gloss_color2.alpha = 0.7;
		
		Gdk.RGBA rim_color  = rgba_from_string ("#898573");
		rim_color.alpha = 0.7;

        int xc = width / 2;
        int yc = height / 2;
        
        int diameter = (width < height ? width : height) - 1;
		double parts = diameter / 20.0;
		double inner_diameter = 10 * parts;

		Cairo.Pattern upper_shine = new Cairo.Pattern.radial(xc - 1.6 * parts, yc - 2.2 * parts, 0.8 * parts, xc, yc, inner_diameter / 2 - 0.5 * parts);
		add_color_stop_to (upper_shine, 0, rgba_shade (base_color, 2.5));
		add_color_stop_to (upper_shine, 0.2, rgba_shade (base_color, 1.5));
		add_color_stop_to (upper_shine, 0.5, rgba_shade (base_color, 1.0));
		add_color_stop_to (upper_shine, 1.0, border_color);

        cr.arc (xc, yc, inner_diameter / 2 - 0.5 * parts, 0, 2 * Math.PI);
        cr.set_source (upper_shine);
        cr.fill ();

		cr.save ();

        cr.arc (xc, yc, inner_diameter / 2 - 0.5 * parts, 0, 2 * Math.PI);
		cr.clip ();

		Cairo.Pattern bottom_gloss = new Cairo.Pattern.radial(xc + 0.68 * 5 * parts, yc + 0.68 * 5 * parts, 1 * parts, xc + 0.68 * 5 * parts, yc + 0.68 * 5 * parts, 4 * parts);
		add_color_stop_to (bottom_gloss, 0, bottom_gloss_color);
		add_color_stop_to (bottom_gloss, 0.3, bottom_gloss_color2);
		add_color_stop_to (bottom_gloss, 1.0, border_color);
		cr.set_source (bottom_gloss);
		cr.arc (xc + 0.68 * 5 * parts, yc + 0.68 * 5 * parts, 3*parts, 0, 2 * Math.PI);
		cr.fill ();

		cr.restore ();

        cr.arc(xc, yc, inner_diameter / 2 - 0.2 * parts, 0, 2 * Math.PI);
		cr.set_line_width (0.7 * parts);
		Gdk.cairo_set_source_rgba (cr, rim_color);
		cr.stroke();

		if (led_state) {
			Cairo.Pattern flare = new Cairo.Pattern.radial(xc, yc, inner_diameter / 2, xc, yc, inner_diameter);
			hsv.saturation = 96;
			hsv.value      = 90;
			Gdk.RGBA flare_color = hsv.to_rgba ();
			flare_color.alpha = 0.3;
			add_color_stop_to (flare, 0.1, flare_color);
			flare_color.alpha = 0.0;
			add_color_stop_to (flare, 1.0, flare_color);

			cr.set_source (flare);
			cr.arc (xc, yc, inner_diameter, 0, 2 * Math.PI);
			cr.fill();
		}
	}

    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context cr) {     
		Gtk.Allocation allocation;
		get_allocation(out allocation);

        set_source_color(cr, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]));
        cr.rectangle(0, 0, allocation.width, allocation.height);
        cr.fill();

		paint_led (cr, base_color, allocation.width, allocation.height, _led_state);
		                
        return true;
    }
}

} // namespace Prolooks
