/* 
    Copyright 2009 by Hans Baier, Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;
using Cairo;

namespace Prolooks {

public interface ILineProperties : GLib.Object {
    public abstract void set_line_color (float r, float g, float b, float a = 1.0f);
    public abstract void set_line_width (float width);
}

public class CairoLineProperties : GLib.Object, ILineProperties {
    public Context context { get; set; }
    public void set_line_color (float r, float g, float b, float a = 1.0f) { context.set_source_rgba (r, g, b, a); }
    public void set_line_width (float width) { context.set_line_width (width); }
}

public interface IGraphSource : GLib.Object {
    /** Obtain subindex'th graph of parameter 'index'
     *   @param index parameter/graph number (usually tied to particular plugin control port)
     *   @param subindex graph number (there may be multiple overlaid graphs for one parameter, eg. for monosynth 2x12dB filters)
     *   @param data buffer for normalized output values
     *   @param points number of points to fill
     *   @param line_props line properties to adjust (for multicolour graphs etc.)
     *   @retval true graph data was returned; subindex+1 graph may or may not be available
     *  @retval false graph data was not returned; subindex+1 graph does not exist either
     */
    public abstract bool get_graph (int index, int subindex, ref float[] data, int points, ref ILineProperties line_props);

    /** Obtain subindex'th dot of parameter 'index'
     *   @param index parameter/dot number (usually tied to particular plugin control port)
     *   @param subindex dot number (there may be multiple dots graphs for one parameter)
     */
    public abstract bool get_dot (int index, int subindex, ref float x, ref float y, ref int size, ref ILineProperties line_props);
    
     /** Obtain subindex'th dot of parameter 'index'
     *   @param index parameter/dot number (usually tied to particular plugin control port)
     *   @param subindex dot number (there may be multiple dots graphs for one parameter)
     */

	/** Defines how the graph line of the given subindex will look like
	*/
    public abstract void set_subindex_look (ILineProperties line_props, int subindex);

    public abstract bool get_gridline (int index, int subindex, ref float pos, ref bool vertical, ref string legend, ref ILineProperties line_props);
    
     /** Obtain subindex'th static graph of parameter index (static graphs are only dependent on parameter value, not plugin state)
     *   @param index parameter/graph number (usually tied to particular plugin control port)
     *   @param subindex graph number (there may be multiple overlaid graphs for one parameter, eg. for monosynth 2x12dB filters)
     *   @param value parameter value to pick the graph for
     *   @param data buffer for normalized output values
     *   @param points number of points to fill
     *   @param line_props line properties to adjust (for multicolour graphs etc.)
     *   @retval true graph data was returned; subindex+1 graph may or may not be available
     *   @retval false graph data was not returned; subindex+1 graph does not exist either
     */
    public abstract bool get_static_graph (int index, int subindex, float value, ref float[] data, int points, ref ILineProperties line_props);
    
     /** Return which graphs need to be redrawn and which can be cached for later reuse
     *   @param generation 0 (at start) or the last value returned by the function (corresponds to a set of input values)
     *   @param subindex_graph First graph that has to be redrawn (because it depends on values that might have changed)
     *   @param subindex_dot First dot that has to be redrawn
     *   @param subindex_gridline First gridline/legend that has to be redrawn
     *   @retval Current generation (to pass when calling the function next time); if different than passed generation value, call the function again to retrieve which graph  offsets should be put into cache
     */
    public abstract int get_changed_offsets (int generation, ref int subindex_graph, ref int subindex_dot, ref int subindex_gridline);
}

public class DefaultGraphSource : GLib.Object, IGraphSource {
    public virtual bool get_graph (int index, int subindex, ref float[] data, int points, ref ILineProperties line_props) {
        return false;
    }

    public virtual bool get_dot (int index, int subindex, ref float x, ref float y, ref int size, ref ILineProperties line_props) {
        return false;
    }
	
	public virtual void set_subindex_look (ILineProperties line_props, int subindex)
    {
        if ((subindex & 1) != 0) {
			RGBA c = rgba_from_string (DisplayBase.text_color_default);
            line_props.set_line_color ((float) c.red, (float) c.green, (float) c.blue);
		} else {
            line_props.set_line_color (0.62f, 0.78f, 0.09f);
		}
        line_props.set_line_width (1.5f);
    }

    public virtual bool get_gridline (int index, int subindex, ref float pos, ref bool vertical, ref string legend, ref ILineProperties line_props) {
        return false;
    }

    public virtual bool get_static_graph (int index, int subindex, float value, ref float[] data, int points, ref ILineProperties line_props)  {
        return false;
    }

    public virtual int get_changed_offsets (int generation, ref int subindex_graph, ref int subindex_dot, ref int subindex_gridline) {
        subindex_graph = subindex_dot = subindex_gridline = 0; 
        return 0;
    }
}

public interface IHasFrequencyResponse : GLib.Object {
    public abstract float freq_gain (int subindex, float freq, float srate);
    public abstract int   sampling_rate ();
}

public class TestFrequencyResponseSource : IHasFrequencyResponse, GLib.Object {
    public float freq_gain (int subindex, float freq, float srate) {
        return freq / srate;
    }
    public int   sampling_rate() {
        return 44100;
    }
}

public class FrequencyResponseGraphSource : DefaultGraphSource {
    public int max_index     { get; set; }
    public int max_subindex  { get; set ;}
    
    construct {
        max_index    = 1;
        max_subindex = 1;
        _freq_response_source = new TestFrequencyResponseSource ();
    }
    
    private IHasFrequencyResponse _freq_response_source;
    
    public IHasFrequencyResponse freq_response_source {
        get {
            return _freq_response_source;
        }
        set {
            _freq_response_source = value;
        }
    }

    public static inline float dB_grid (float amp) {
        return Math.logf (amp) / Math.logf (256.0f) + 0.4f;
    }

    public static bool get_freq_graph (IHasFrequencyResponse fx, int subindex, ref float[] data, int points)
    {
        for (int i = 0; i < points; i++)
        {
            double freq = 20.0f * Math.pow (20000.0f / 20.0f, i * 1.0f / (float)points);
            data[i] = dB_grid (fx.freq_gain (subindex, (float)freq, fx.sampling_rate ()));
        }
        return true;
    }
        
    public override bool get_graph (int index, int subindex, ref float[] data, int points, ref ILineProperties line_props) {
        if (_freq_response_source != null && index <= max_index && subindex <= max_subindex) {
            return get_freq_graph (_freq_response_source, subindex, ref data, points);
        } else {
            return false;
        }
    }

    public override bool get_gridline (int index, int subindex, ref float pos, ref bool vertical, ref string legend, ref ILineProperties line_props) { 
        return get_gridline_use (index, subindex, ref pos, ref vertical, ref legend, ref line_props, true); 
    }
    
    public bool get_gridline_use (int index, int subindex, ref float pos, ref bool vertical, ref string legend, ref ILineProperties line_props, bool use_frequencies = true) { 
        if (subindex < 0 )
        return false;
        
        if (use_frequencies)
        {
            if (subindex < 28)
            {
                vertical = true;
                if (subindex == 9) legend = "100 Hz";
                if (subindex == 18) legend = "1 kHz";
                if (subindex == 27) legend = "10 kHz";
                float freq = 100;
                if (subindex < 9)
                    freq = 10 * (subindex + 1);
                else if (subindex < 18)
                    freq = 100 * (subindex - 9 + 1);
                else if (subindex < 27)
                    freq = 1000 * (subindex - 18 + 1);
                else
                    freq = 10000 * (subindex - 27 + 1);
                pos = Math.logf (freq / 20.0f) / Math.logf (1000f);
                if (legend.length != 0)
                    line_props.set_line_color (0.25f, 0.25f, 0.25f, 0.75f);
                else
                    line_props.set_line_color (0.25f, 0.25f, 0.25f, 0.5f);
                return true;
            }
            subindex -= 28;
        }
        if (subindex >= 32)
            return false;
        float gain = 16.0f / (float) (1 << subindex);
        pos = dB_grid (gain);
        if (pos < -1)
            return false;
        if (subindex != 4)
            line_props.set_line_color (0.25f, 0.25f, 0.25f, ((subindex & 1) != 0) ? 0.5f : 0.75f);
        if ((subindex & 1) == 0)
        {
            legend = "%d dB".printf (24 - 6 * subindex);
        }
        vertical = false;
        return true;
    }

    int get_changed_offsets (int generation, ref int subindex_graph, ref int subindex_dot, ref int subindex_gridline)
    {
        subindex_graph = 0;
        subindex_dot = 0;
        subindex_gridline = (generation != 0) ? int.MAX : 0;
        return 1;
    }
}

public class LineGraph : DrawingArea {

    public  IGraphSource source { get; set; }
    
    private int          source_id;
    private bool         is_square;
    private Surface?     cache_surface;
    private int          last_generation;

    construct {
        set_size_request (50, 50);
        cache_surface = null;
        last_generation = 0;
        var src = new FrequencyResponseGraphSource();
		source = src;
		draw.connect(on_draw);
		size_allocate.connect(on_size_allocate);
    }

    private void copy_cache_to_window (Context c) {
        c.save ();
        c.set_source_surface (cache_surface, 0, 0);
        c.paint ();
        c.restore ();
    }

    /* unused
    private void copy_window_to_cache (Context c) {
        Context cache_cr = new Context (cache_surface);
        Surface window_surface = c.get_target ();
        cache_cr.set_source_surface (window_surface, 0, 0);
        cache_cr.paint ();
    }
    */

    public static void draw_grid (Context c, ref string legend, bool vertical, float pos, int phase, int sx, int sy) {
        int ox = 1, oy = 1;
        TextExtents tx = TextExtents ();

        if (legend.length != 0) {
            c.text_extents (legend, out tx);
        }

        if (vertical)
        {
            float x = Math.floorf (ox + pos * sx) + 0.5f;

            if (phase == 1)
            {
                c.move_to (x, oy);
                c.line_to (x, oy + sy);
                c.stroke ();
            }

            if (phase == 2 && (legend.length != 0)) {

                c.set_source_rgba (1.0, 1.0, 1.0, 0.75);
                c.move_to (x - (tx.x_bearing + tx.width / 2.0), oy + sy - 2);
                c.show_text (legend);
            }
        }
        else
        {
            float y = Math.floorf (oy + sy / 2 - (sy / 2 - 1) * pos) + 0.5f;

            if (phase == 1)
            {
                c.move_to (ox, y);
                c.line_to (ox + sx, y);
                c.stroke ();
            }

            if (phase == 2 && (legend.length != 0)) {
                c.set_source_rgba (1.0, 1.0, 1.0, 0.75);
                c.move_to (ox + sx - 2 - tx.width, y + tx.height/2 - 1);
                c.show_text (legend);
            }
        }
    }


    static void draw_graph (Context c, ref float[] data, int sx, int sy) {
        int ox=1, oy=1;

        for (int i = 0; i < 2 * sx; i++) 
        {
            int y = (int) (oy + sy / 2 - (sy / 2 - 1) * data[i]);
            //if (y < oy) y = oy;
            //if (y >= oy + sy) y = oy + sy - 1;
            if (i != 0) {
                c.line_to (ox + i * 0.5, y);
            } else {
                c.move_to (ox, y);
            }
        }
        c.stroke ();
    }

	public bool on_draw (Cairo.Context c) {     
		Gtk.Allocation allocation;
		get_allocation(out allocation);

		int ox = 1, oy = 1;
        int sx = allocation.width - 2, sy = allocation.height - 2;
    
        Gdk.RGBA sc = { 0, 0, 0 };

        bool cache_dirty = false;

        if (cache_surface == null) {
            // looks like its either first call or the widget has been resized.
            // create the cache_surface.
            Surface window_surface = c.get_target ();
            cache_surface = new Surface.similar (window_surface, 
                                                 Content.COLOR,
                                                 allocation.width,
                                                 allocation.height);

			//Context cache_cr = new Context (cache_surface);
            //cache_cr.set_source_surface (window_surface, 0,0);
            //cache_cr.paint ();
    
            cache_dirty = true;
        }

        c.select_font_face ("Bitstream Vera Sans", FontSlant.NORMAL, FontWeight.NORMAL);
        c.set_font_size (9);

        cairo_set_source_rgba (c, sc);
        c.rectangle (ox, oy, sx, sy);
        c.clip ();

        CairoLineProperties cairoimpl = new CairoLineProperties ();
        cairoimpl.context = c;
		ILineProperties cimpl = cairoimpl;

        if (source != null) {
            float pos = 0;
            bool vertical = false;
            string legend;
            float[] data = new float[2 * sx];
            Gdk.RGBA sc2 = { 0, 1.0, 0 };
            float x = 0, y = 0;
            int size = 0;
            Gdk.RGBA sc3 = { 0.5, 1.0, 0.0 };

            int graph_n = 0, grid_n = 0, dot_n = 0, grid_n_save = 0;

            int cache_graph_index = 0, cache_dot_index = 0, cache_grid_index = 0;
            int gen_index = source.get_changed_offsets (last_generation, ref cache_graph_index, ref cache_dot_index, ref cache_grid_index);

            if (cache_dirty || (gen_index != last_generation)) {
                Context cache_cr = new Context (cache_surface);
                cache_cr.select_font_face ("Bitstream Vera Sans", FontSlant.NORMAL, FontWeight.NORMAL);
                cache_cr.set_font_size (9);

                cairo_set_source_rgba (cache_cr, sc);
                cache_cr.rectangle (ox, oy, sx, sy);
                cache_cr.clip_preserve ();
                cache_cr.fill ();

				{ //Display
					bool show_matrix = true;
					bool show_glass_rim = false;
					double inner_width  = show_glass_rim ? (sx - 14) : (sx - 6);
					double inner_height = show_glass_rim ? (sy - 13) : (sy - 6);
					cache_cr.save();

					set_line_width_from_device (cache_cr);

					DisplayBase.outer_rim_for_display (cache_cr, 0, 0, sx, sy, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]));
            
					if (show_glass_rim) {
						cache_cr.translate (4.5, 3.5);
						DisplayBase.inner_glass_rim (cache_cr, 0, 0, sx - 8, sy - 9, 2);
					}
            
					cache_cr.translate (3.0, 3.0);
            
					DisplayBase.lcd(cache_cr, inner_width, inner_height, sy > 30 ? 5.0 : 3.0);
            
					if (show_matrix) {
						DisplayBase.dot_matrix (cache_cr, show_glass_rim ? 0 : 0.5, show_glass_rim ? 0 : 0.5, inner_width, inner_height, DisplayBase.matrix_dot_color ());
					}

					DisplayBase.reflection(cache_cr, inner_width, inner_height, inner_height / 2.0);

					cache_cr.restore();
				}


                CairoLineProperties cache_cairoimpl = new CairoLineProperties ();
                cache_cairoimpl.context = cache_cr;
				ILineProperties cache_cimpl = cache_cairoimpl;

                source.get_changed_offsets (gen_index, ref cache_graph_index, ref cache_dot_index, ref cache_grid_index);
                last_generation = gen_index;

                cache_cr.set_line_width (1);
                for (int phase = 1; phase <= 2; phase++)
                {
                    for (grid_n = 0;; grid_n++)
                    {
                        legend = "";
                        cache_cr.set_source_rgba (1, 1, 1, 0.5);
                        if ((grid_n < cache_grid_index) &&  
                            source.get_gridline (source_id, grid_n, ref pos, ref vertical, ref legend, ref cache_cimpl)) {
                            draw_grid (cache_cr, ref legend, vertical, pos, phase, sx, sy);
                        } else {
                            break;
                        }
                    }
                }
                grid_n_save = grid_n;

                cairo_set_source_rgba (cache_cr, sc2);
                cache_cr.set_line_join (LineJoin.MITER);
                cache_cr.set_line_width (1);

                for (graph_n = 0; 
                     (graph_n<cache_graph_index) && 
                         source.get_graph (source_id, graph_n, ref data, 2 * sx,  ref cache_cimpl); 
                     graph_n++)
                {
                    draw_graph (cache_cr, ref data, sx, sy);
                }

                cairo_set_source_rgba (cache_cr, sc3);

                for (dot_n = 0;; dot_n++)
                {
                    size = 3;
                    if ((dot_n<cache_dot_index) && 
                        source.get_dot (source_id, dot_n, ref x, ref y, ref size, ref cache_cimpl)) {

                        int yv = (int) (oy + sy / 2 - (sy / 2 - 1) * y);
                        cache_cr.arc (ox + x * sx, yv, size, 0, 2 * Math.PI);
                        cache_cr.fill ();
                    } else {
                        break;
                    }
                }

                cache_cr = null;
                copy_cache_to_window (c);

            } else {
                grid_n_save = cache_grid_index;
                graph_n = cache_graph_index;
                dot_n = cache_dot_index;
                copy_cache_to_window (c);
            }

            c.set_line_width (1);

            for (int phase = 1; phase <= 2; phase++)
            {
                for (int gn = grid_n_save;; gn++)
                {
                    legend = ""; 
                    c.set_source_rgba (1, 1, 1, 0.5);
                    if (source.get_gridline (source_id, gn, ref pos, ref vertical, ref legend, ref cimpl)) {
                        draw_grid (c, ref legend, vertical, pos, phase, sx, sy);
                    } else {
                        break;
                    }
                }
            }

            cairo_set_source_rgba (c, sc2);
            c.set_line_join (LineJoin.MITER);
            c.set_line_width (1);

            for (int gn = graph_n; 
                 source.get_graph (source_id, gn, ref data, 2 * sx, ref cimpl); 
                 gn++)
            {
				source.set_subindex_look (cimpl, gn);
                draw_graph (c, ref data, sx, sy);
            }

            cairo_set_source_rgba (c, sc3);
            for (int gn = dot_n;; gn++)
            {
                size = 3;
                if (source.get_dot (source_id, gn, ref x, ref y, ref size, ref cimpl)) {
                    int yv = (int) (oy + sy / 2 - (sy / 2 - 1) * y);
                    c.arc (ox + x * sx, yv, size, 0, 2 * Math.PI);
                    c.fill ();
                } else {
                    break;
                }
            }
        }
    
    
		get_style_context().render_frame(c, ox - 1, oy - 1, sx + 2, sy + 2);
        //stderr.printf ("exposed %p %dx%d %d+%d\n", window, event.area.x, event.area.y, event.area.width, event.area.height);
    
        return true;
    }

    /* unused
    int update_if (int last_drawn_generation)
    {
        int generation = last_drawn_generation;
        if (source != null)
        {
            int subgraph = 0, dot = 0, gridline = 0;
            generation = source.get_changed_offsets (generation, ref subgraph, ref dot, ref gridline);
            if (subgraph == int.MAX && dot == int.MAX && gridline == int.MAX && generation == last_drawn_generation) {
                return generation;
            }

            queue_draw ();
        }
        return generation;
    }
    */

    public void on_size_allocate (Gtk.Allocation allocation) {
		set_allocation(allocation);
    
		Gtk.Allocation a = allocation;

        if (is_square)
        {
            if (a.width > a.height)
            {
                a.x += (a.width - a.height) / 2;
                a.width = a.height;
            }
            if (a.width < a.height)
            {
                a.y += (a.height - a.width) / 2;
                a.height = a.width;
            }
        }

        base.size_allocate (a);
        cache_surface = null;
    }
}

} // namespace Prolooks
