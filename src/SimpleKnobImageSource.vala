using Gtk;
using Cairo;

namespace Prolooks {
	protected class SimpleKnobImageSource : DrawingArea, IKnobImageSource {
		public double knob_width   { get; set; }
		public double knob_height  { get; set; }
		public double x            { get; set; }
		public double y            { get; set; }
		public Gdk.RGBA led_color  { get; set; }

		public double get_knob_width () { 
			return knob_width;
		}

		public double get_knob_height () {
			return knob_height;
		}

		public double get_line_width () {
			return knob_width / 10;
		}

		public double get_radius () { 
			return knob_width / 2 - get_line_width ();
		}

		public string to_string () { 
			return "SimpleKnobImageSource: Dimens: (%f, %f); linewidth: %f; radius: %f; led-color: %s".printf (
				get_knob_width (), 
				get_knob_height (), 
				get_line_width (), 
				get_radius (), 
				led_color.to_string());
		}

		construct {
			knob_width = 40;
			knob_height = 40;
			x = knob_width / 2;
			y = knob_height / 2;
			set_size_request (phases * (int)knob_width, variants * (int)knob_height);
			led_color = rgba_from_string ("#ff7f00");
			draw.connect(on_draw);
		}
		
		public bool on_draw (Cairo.Context cr) {        
			cr.set_source_rgba(0.75, 0.75, 0.75, 0);
			cr.rectangle(0, 0, phases * knob_width, 4 * knob_height);
			cr.fill();

			for (int variant = 0; variant <= variants; variant++) {
				// x/y = the middle of the knob
				var x = knob_width / 2;
				var y = knob_height * (variant + 0.5);
				paint_knobs (cr, (KnobMode)variant, x, y);
			}
				
			return true;
		}

		public void paint_knob (Cairo.Context cr, KnobMode knob_mode, int phase,  double lwidth, double radius, double x, double y) {
			var radiusminus2 = radius - lwidth;
			var radiusminus3 = radius - lwidth * 3 / 2;
			var value = 0.7;
			
			var sangle = 0.0;
			var eangle = 0.0;
			var vangle = 0.0;
			var nleds  = 0.0;

			// Draw out the triangle using absolute coordinates
			value = phase * 1.0 / (phases - 1);
			if (knob_mode != KnobMode.ENDLESS) {
				sangle = (180 - 45) * Math.PI / 180;
				eangle = (360 + 45) * Math.PI / 180;
				nleds = 31;
			} else {
				sangle = (270) * Math.PI / 180;
				eangle = (270 + 360) * Math.PI / 180;
				nleds = 32;
			}

			vangle = sangle + value * (eangle - sangle);
			var c = Math.cos(vangle);
			var s = Math.sin(vangle);

			var midled = (nleds - 1) / 2;
			var midphase = (phases - 1) / 2;
			var thresholdP = midled + 1 + ((phase - midphase - 1) * 1.0 * (nleds - midled - 2) / (phases - midphase - 2));
			var thresholdN = midled - 1 - ((midphase - 1 - phase) * 1.0 * (nleds - midled - 2) / (midphase - 1));
	
			var spacing = Math.PI / nleds;
			for (var led = 0; led < nleds; led++) {
				var adelta = 0.0;

				if (knob_mode == KnobMode.ENDLESS) {
					adelta = (eangle - sangle) / (nleds);
				} else {
					adelta = (eangle - sangle - spacing) / (nleds - 1);
				}

				var lit = false;
				var glowlit = false;
				var glowval = 0.5;
				var hilite = false;
				var lvalue = led   * 1.0 / (nleds  - 1);
				var pvalue = phase * 1.0 / (phases - 1);

				if (knob_mode == KnobMode.ENDLESS) {
					// XXXKF works only for phases = 2 * leds
					var exled = phase / 2.0;
					lit = (led == exled) || (phase == (phases - 1) && led == 0);
					glowlit = led == (exled + 0.5) || led == (exled - 0.5);
					glowval = 0.8;
					hilite = (phase % ((phases - 1) / 4)) == 0;
				}

				if (knob_mode == KnobMode.POSITIVE) {
					lit = (pvalue == 1.0) || pvalue > lvalue;
				}

				if (knob_mode == KnobMode.BIPOLAR) {
					if (led == midled) {
						lit = (phase == midphase);
						//glowlit = (phase < midphase && thresholdN >= midled - 1) || (phase > midphase and thresholdP <= midled + 1)
						glowlit = false;
						hilite = true;
					} else if (led > midled && phase > midphase) {
						// led = [midled + 1, nleds - 1];
						// phase = [midphase + 1, phases - 1];
						lit = led <= thresholdP;
						glowlit = led <= thresholdP + 1;
						glowval = 0.4;
					} else if (led < midled && phase < midphase) {
						lit = led >= thresholdN;
						glowlit = led >= thresholdN - 1;
						glowval = 0.4;
					} else {
						lit = false;
					}
				}

				if (knob_mode == KnobMode.NEGATIVE) {
					lit = pvalue == 0 || pvalue < lvalue;
				}

				if (!lit) {
					if (!glowlit) {
						cr.set_source_rgb(0, 0, 0);
					} else {
						Gdk.cairo_set_source_rgba(cr, rgba_shade (led_color, glowval));
					} 
				} else {
					if (hilite) {
						cr.set_source_rgb(1, 1, 0);
					} else {
						Gdk.cairo_set_source_rgba(cr, led_color);
					}
				}

				cr.set_line_width(3);
				cr.arc(x, y, radius, sangle + adelta * led, sangle + adelta * led + spacing);
				cr.stroke();
			}

			// knob fill
			var grad = new Cairo.Pattern.linear(x - radius / 2, y - radius / 2, x + radius / 2, y + radius / 2);
			grad.add_color_stop_rgb(0.0, 0.5, 0.5, 0.5);
			grad.add_color_stop_rgb(1.0, 0.8, 0.8, 0.8);
			cr.set_source(grad);
			cr.set_line_width(2);
			cr.arc(x, y, radiusminus2, 0, 2 * Math.PI);
			cr.fill();

			// knob fill
			grad = new Cairo.Pattern.linear(x - radius / 2, y - radius / 2, x + radius / 2, y + radius / 2);
			grad.add_color_stop_rgb(0.0, 0.8, 0.8, 0.8);
			grad.add_color_stop_rgb(1.0, 0.5, 0.5, 0.5);
			cr.set_source(grad);
			cr.arc(x, y, radiusminus3, 0, 2 * Math.PI);
			cr.fill();
			
			// knob border
			cr.set_source_rgb(0, 0, 0);
			cr.set_line_width(2);
			cr.arc(x, y, radiusminus2, 0, 2 * Math.PI);
			cr.stroke();

			// knob line
			cr.set_source_rgba(0, 0, 0, 0.5);
			cr.set_line_width(1);
			var mtx = Matrix (0,0,0,0,0,0);
			cr.get_matrix(out mtx);
			cr.translate(x + radiusminus2 * c, y + radiusminus2 * s);
			cr.rotate(vangle);
			cr.move_to(0, 0);
			cr.line_to(-radius/2, 0);
			cr.stroke();
			cr.set_matrix(mtx);
			
			// knob shine
			cr.set_source_rgba (1, 1, 1, 0.15);
			cr.new_path();
			cr.arc_negative(x, y, radiusminus3, -Math.PI / 6.0, Math.PI);
			cr.close_path();
			cr.fill();
		} 
	} 
} // namespace Prolooks