/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

// template for new widgets

namespace Prolooks {

public class SmallButton : ButtonBase {    
    private RGBA base_color;

    [Description(nick="button color", blurb="color as a string as accepted by gdk_color_parse (), eg: #a0b1c2")]
    public string color {
        set {
            lock (base_color) {
                if (value != null & value != "") {
                    base_color = rgba_from_string (value);
                } else {
                    base_color = gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]);
                }
            }
        }
    }
    
    public SmallButton () {

        // Enable the events you wish to get notified about.
        // The 'expose' event is already enabled by the DrawingArea.
        add_events (Gdk.EventMask.BUTTON_PRESS_MASK
                  | Gdk.EventMask.BUTTON_RELEASE_MASK
                  | Gdk.EventMask.ENTER_NOTIFY_MASK
                  | Gdk.EventMask.LEAVE_NOTIFY_MASK);

        base_color = gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]);
        
        // Set favored widget size
        set_size_request (20, 20);
        
		draw.connect(on_draw);
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width  = 20;		
        natural_width  = 20;		
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 20;
		natural_height = 20;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 20;
		natural_height = 20;
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = 20;		
        natural_width  = 20;				
	}

    
    public static void button_bg (Cairo.Context cr, Gdk.RGBA base_color, Gdk.RGBA outside_color, double width, double height, double shade_inside = 1.0) {
        Gdk.RGBA color;
        
        // line
        
        color = outside_color; color = rgba_shade (color, 1.06);  set_source_color (cr, color);
        
        cr.move_to (2, 0); cr.line_to (width - 3, 0); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.05);  set_source_color (cr, color);
        
        cr.move_to (1, 1); cr.line_to (width - 2, 1); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.05);  set_source_color (cr, color);
        
        cr.move_to (0, 2); cr.line_to (width - 1, 2); cr.stroke ();
        // line
        
        color = outside_color; color = rgba_shade (color, 1.04);  set_source_color (cr, color);
        
        cr.move_to (3, 0); cr.line_to (width - 4, 0); cr.stroke ();


        // line
        
        color = outside_color; color = rgba_shade (color, 0.97);  set_source_color (cr, color);
        
        cr.move_to (2, 1); cr.line_to (width - 3, 1); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 0.96);  set_source_color (cr, color);
        
        cr.move_to (1, 2); cr.line_to (width - 2, 2); cr.stroke ();

        // missing line
        
        color = outside_color; color = rgba_shade (color, 0.96);  set_source_color (cr, color);
        
        cr.move_to (0, 3); cr.line_to (width - 1, 3); cr.stroke ();



        // line
        
        color = outside_color; color = rgba_shade (color, 1.13);  set_source_color (cr, color);
        
        cr.move_to (2, height - 1); cr.line_to (width - 3, height - 1); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.09);  set_source_color (cr, color);
        
        cr.move_to (1, height - 2); cr.line_to (width - 2, height - 2); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.10);  set_source_color (cr, color);
        
        cr.move_to (0, height - 3); cr.line_to (width - 1, height - 3); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.15);  set_source_color (cr, color);
        
        cr.move_to (3, height - 1); cr.line_to (width - 4, height - 1); cr.stroke ();


        // line
        
        color = outside_color; color = rgba_shade (color, 1.12);  set_source_color (cr, color);
        
        cr.move_to (2, height - 2); cr.line_to (width - 3, height - 2); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.13);  set_source_color (cr, color);
        
        cr.move_to (1, height - 3); cr.line_to (width - 2, height - 3); cr.stroke ();

        ///////////////////////////  border outline 
        // line
        
        color = base_color; color = rgba_shade (color, 0.6);  set_source_color (cr, color);
        
        cr.move_to (3, 1); cr.line_to (width - 4, 1); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.6);  set_source_color (cr, color);
        
        cr.move_to (3, height - 2); cr.line_to (width - 4, height - 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.6);  set_source_color (cr, color);
        
        cr.move_to (1, 3); cr.line_to (1, height - 4); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.6);  set_source_color (cr, color);
        
        cr.move_to (width - 2, 3); cr.line_to (width - 2, height - 4); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.6);  set_source_color (cr, color);
        
        cr.move_to (2, 2); cr.line_to (width - 3, 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.6);  set_source_color (cr, color);
        
        cr.move_to (2, height - 3); cr.line_to (width - 3, height - 3); cr.stroke ();


        ///////////////////////////  border smooth effect 
        // line
        
        color = base_color; color = rgba_shade (color, 1.02);  set_source_color (cr, color);
        
        cr.move_to (3, 2); cr.line_to (width - 4, 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 1.00);  set_source_color (cr, color);
        
        cr.move_to (2, 3); cr.line_to (2, height - 4); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.94);  set_source_color (cr, color);
        
        cr.move_to (width - 3, 3); cr.line_to (width - 3, height - 4); cr.stroke ();


        ///////////////////////////  inside highlight 
        // line
        
        color = base_color; color = rgba_shade (color, 1.2);  set_source_color (cr, color);
        
        cr.move_to (4, 2); cr.line_to (width - 5, 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 1.1);  set_source_color (cr, color);
        
        cr.move_to (2, 4); cr.line_to (2, height - 5); cr.stroke ();

        ///////////////////////////  inside shadow 
        // line
        
        color = base_color; color = rgba_shade (color, 1.04);  set_source_color (cr, color);
        
        cr.move_to (width - 3, 4); cr.line_to (width - 3, height - 5); cr.stroke ();


        ///////////////////////////  fill gradient 
        // gradient

        Cairo.Pattern gloss_gradient = gloss_gradient_pattern(0.0, 0.0, 0.0, height, rgba_shade(base_color, shade_inside));
        
        cr.set_source (gloss_gradient);
        cr.rectangle (1.0, 2, width - 4, height - 5);
        cr.fill ();


        ///////////////////////////  bottom border smooth effect 
        // line
        
        color = base_color; color = rgba_shade (color, 1.00);  set_source_color (cr, color);
        
        cr.move_to (3, height - 3); cr.line_to (width - 4, height - 3); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 1.1);  set_source_color (cr, color);
        
        cr.move_to (4, height - 3); cr.line_to (width - 5, height - 3); cr.stroke ();
    }
    
    public static void button_bg_pressed (Cairo.Context cr, Gdk.RGBA base_color, Gdk.RGBA outside_color, double width, double height) {
        Cairo.Pattern gradient;
        Gdk.RGBA color;
        //Gdk.RGBA test_color = RGBA.parse ("#ff0000");
        
        ///////////////////////////  outside highlight 
        // gradient
        
        gradient = create_gradient (0, 0, 0, height - 4, rgba_shade (outside_color, 1.2), rgba_shade (outside_color, 1.0));
        
        cr.set_source (gradient);
        cr.rectangle (width - 2 - 0.5, 2, 1, height - 4);
        cr.fill ();

        // gradient
        
        gradient = create_gradient (0, 0, 0, height - 6, rgba_shade (outside_color, 1.2), rgba_shade (outside_color, 1.0));
        
        cr.set_source (gradient);
        cr.rectangle (width - 1 - 0.5, 3, 1, height - 6);
        cr.fill ();

        // line

        
        color = outside_color; color = rgba_shade (color, 1.0);  set_source_color (cr, color);
        
        cr.move_to (2, height - 2); cr.line_to (width - 3, height - 2); cr.stroke ();

        // line
        
        color = outside_color; color = rgba_shade (color, 1.0);  set_source_color (cr, color);
        
        cr.move_to (3, height - 1); cr.line_to (width - 4, height - 1); cr.stroke ();

        ///////////////////////////  border outline 
        // line
        
        color = base_color; color = rgba_shade (color, 0.55);  set_source_color (cr, color);
        
        cr.move_to (3, 1); cr.line_to (width - 4, 1); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.55);  set_source_color (cr, color);
        
        cr.move_to (3, height - 2); cr.line_to (width - 4, height - 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.55);  set_source_color (cr, color);
        
        cr.move_to (1, 3); cr.line_to (1, height - 4); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.55);  set_source_color (cr, color);
        
        cr.move_to (width - 2, 3); cr.line_to (width - 2, height - 4); cr.stroke ();


        // line
        
        color = base_color; color = rgba_shade (color, 0.55);  set_source_color (cr, color);
        
        cr.move_to (2, 2); cr.line_to (width - 3, 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.55);  set_source_color (cr, color);
        
        cr.move_to (2, height - 3); cr.line_to (width - 3, height - 3); cr.stroke ();


        ///////////////////////////  inside shadow 
        // line
        
        color = base_color; color = rgba_shade (color, 0.9);  set_source_color (cr, color);
        
        cr.move_to (3, 2); cr.line_to (width - 4, 2); cr.stroke ();

        // line
        
        color = base_color; color = rgba_shade (color, 0.85);  set_source_color (cr, color);
        
        cr.move_to (2, 3); cr.line_to (2, height - 4); cr.stroke ();


        ///////////////////////////  fill gradient 
        // gradient
        
        gradient = create_gradient (0, 0, 0, height - 6, rgba_shade (base_color, 0.95), rgba_shade (base_color, 0.9));
        
        cr.set_source (gradient);
        cr.rectangle (3 - 0.5, 2, width - 5, height - 5.5);
        cr.fill ();
        // line
        
        color = base_color; color = rgba_shade (color, 0.9);  set_source_color (cr, color);
        
        cr.move_to (3, height - 3); cr.line_to (width - 5, height - 3); cr.stroke ();


    }

    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context cr) {        
		Gtk.Allocation allocation;
		get_allocation(out allocation);

        double width  = allocation.width - 2;
        double height = allocation.height - 2;
        
        cr.set_line_width (1);
        cr.set_line_cap (Cairo.LineCap.SQUARE);

        cr.translate (1.5,1.5);

		if (button_state == ButtonState.PRESSED) {
			lock (base_color) {
				button_bg_pressed (cr, base_color, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]), width, height);
			}
		} else {
			lock (base_color) {
				button_bg (cr, base_color, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]), width, height, prelighted ? 1.1 : 1.0);
			}
		}
        return false;
    }

}

} // namespace Prolooks
