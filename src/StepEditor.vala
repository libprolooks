using Gtk;
using Gdk;

namespace Prolooks {

public class StepEditor : DrawingArea {

    private float [] _step_values;
    private uint _steps;
    public uint steps {
        set { 
            _steps = value;
            float[] oldsteps = _step_values;
            _step_values = new float[_steps];
            if (oldsteps != null) {
                for (int i = 0; i < oldsteps.length && i < _step_values.length; i++)
                    _step_values[i] = oldsteps[i];
            }
            queue_draw();
        }
        get { return _steps; }
    }    

    private uint _spacing;
    public uint spacing {
        set { _spacing = value; queue_draw();} 
        get { return _spacing; }
    }

    protected int edited_step = -1;
    
    private float first_step = 0.0f;
    
    construct {
        _steps = 16;
        _spacing = 1;
        
        // The 'expose' event is already enabled by the DrawingArea.
        add_events (Gdk.EventMask.BUTTON_PRESS_MASK
                  | Gdk.EventMask.BUTTON_RELEASE_MASK
                  | Gdk.EventMask.POINTER_MOTION_MASK);
        
		draw.connect(on_draw);
    }
    

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width  = (int)(_steps * 12 - _spacing);
        natural_width  = minimum_width;
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = (int)(_steps * 12 - _spacing);
        natural_width  = minimum_width;
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 64;
		natural_height = 64;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 64;
		natural_height = 64;
	}

    /* Widget is asked to draw itself */
	public bool on_draw (Cairo.Context cr) {        
		Gtk.Allocation allocation;
		get_allocation(out allocation);
        int w = allocation.width;
        int h = allocation.height;
        
        cr.set_source_rgba(0.0, 0.0, 0.0, 1);
        cr.rectangle(allocation.x, allocation.y, w, h);
        cr.fill();
        
        for (int i = 0; i < steps; i++) {
            int pos = (int)(h * get_step(i));
            int p1 = (int)(allocation.x + i * (w + _spacing) / _steps);
            int p2 = (int)(allocation.x + (i + 1) * (w + _spacing) / _steps - _spacing);
            cr.set_source_rgba(0.1, 0.1, 0.1, 1);
            cr.rectangle(p1, allocation.y, p2 - p1, h - pos);
            cr.fill();
            cr.set_source_rgba(0.2, 0.5, 0.3, 1);
            cr.rectangle(p1, allocation.y + h - pos, p2 - p1, pos);
            cr.fill();
        }

        // ...

        return false;
    }

    /* Mouse button got pressed over widget */
    public override bool button_press_event (Gdk.EventButton event) {
		Gtk.Allocation allocation;
		get_allocation(out allocation);

        edited_step = (int)(event.x * _steps / allocation.width);
        set_step(edited_step, (float)(1.0 - event.y / allocation.height));
        grab_focus ();
        grab_add (this);
        queue_draw();
        // ...
        return false;
    }

    /* Mouse button got released */
    public override bool button_release_event (Gdk.EventButton event) {
        if (has_grab ())
            grab_remove (this);
        edited_step = -1;
        return false;
    }

    /* Mouse pointer moved over widget */
    public override bool motion_notify_event (Gdk.EventMotion event) {
		Gtk.Allocation allocation;
		get_allocation(out allocation);

        if (edited_step != -1)
            set_step(edited_step, (float)(1.0 - event.y / allocation.height));
        // ...
        return false;
    }
    
    public float get_step(uint step)
    {
        return _step_values[step];
    }
    
    public void set_step(uint step, float value)
    {
        if (value < 0.0f)
            value = 0.0f;
        if (value > 1.0f)
            value = 1.0f;
        _step_values[step] = value;
        queue_draw();
    }
}

} // namespace Prolooks
