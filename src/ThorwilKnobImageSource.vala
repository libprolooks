using Gtk;
using Cairo;

namespace Prolooks {
	protected class ThorwilKnobImageSource : DrawingArea, IKnobImageSource {
		public double knob_width    { get; set; }
		public double knob_height   { get; set; }

		private HSV       lamp_hsv;
		private Gdk.RGBA  _lamp_color;

		public Gdk.RGBA lamp_color { 
			get {
				return _lamp_color;
			}

			set {
				_lamp_color = value;
				lamp_hsv = new HSV.for_rgba (value);
			}
		}

		public double get_knob_width () { 
			return knob_width;
		}

		public double get_knob_height () {
			return knob_height;
		}

		public double get_line_width () {
			return 0.0;
		}

		public double get_radius () { 
			return knob_width / 2.0;
		}

		public string to_string () { 
			return "ThorwilKnobImageSource: Dimens: (%f, %f); radius: %f; lamp-hue: %f".printf (
				get_knob_width (), 
				get_knob_height (), 
				get_radius (), 
				lamp_hsv.hue);
		}

		construct {
			knob_width  = 40;
			knob_height = 40;
			set_size_request ((int)(phases * knob_width), (int) (knob_height));
			lamp_color = rgba_from_string ("#b9feff");
			draw.connect(on_draw);
		}
		
		public bool on_draw (Cairo.Context cr) {        
			cr.set_source_rgba(0.75, 0.75, 0.75, 0);

			cr.rectangle(0, 0, phases * knob_width, 4 * knob_height);
			cr.fill();

			for (int variant = 0; variant < 1; variant++) {
				// x/y = the middle of the knob
				var x = get_radius();
				var y = knob_height * (variant + 0.5);
				paint_knobs (cr, (KnobMode)variant, x, y);
			}

			return true;
		}

		public void paint_knob (Cairo.Context cr, KnobMode knob_mode, int phase,  double lwidth, double radius, double x, double y) {
			var width  = 105.0;
			var height = 105.0;
			var xc = width  / 2.0;
			var yc = height / 2.0;
			
			var start_angle = 0.0;
			var end_angle   = 0.0;
			var value_angle = 0.0;

			// Draw out the triangle using absolute coordinates
			var value = phase * 1.0 / (phases - 1);

			if (knob_mode != KnobMode.ENDLESS) {
				start_angle = (180 - 45) * Math.PI / 180;
				end_angle   = (360 + 45) * Math.PI / 180;
			} else {
				start_angle = (270) * Math.PI / 180;
				end_angle   = (270 + 360) * Math.PI / 180;
			}

			value_angle = start_angle + value * (end_angle - start_angle);

			var value_x       = Math.cos(value_angle);
			var value_y       = Math.sin(value_angle);
			var start_angle_x = Math.cos(start_angle);
			var start_angle_y = Math.sin(start_angle);
			var end_angle_x   = Math.cos(end_angle);
			var end_angle_y   = Math.sin(end_angle);

			cr.save ();
			cr.translate (x, 0);
			cr.scale (2.0 * radius / width, 2.0 * radius / height);
			cr.translate (-xc, 0);

			// KnobRim
			cr.set_source (create_gradient_str (32, 16, 75, 16, "#d4c8b9", "#ae977b"));
			cr.set_line_width (2.0);
			cr.arc (xc, yc, 31.5, 0.0, 2 * Math.PI);
			cr.stroke ();

			var progress_width  = 20.0;
			var progress_radius = 40.0;
			var progress_radius_inner = progress_radius - progress_width / 2.0;
			var progress_radius_outer = progress_radius + progress_width / 2.0;
			var knob_disc_radius      = progress_radius_inner - 5.0;

			// ProgressBackground
			cr.set_source (create_gradient_str (20, 20, 89, 87, "#2f2f4c", "#090a0d"));
			cr.set_line_width (progress_width);
			cr.arc (xc, yc, progress_radius, start_angle, end_angle);
			cr.stroke ();

			// ProgressLamp
			lamp_hsv.saturation = 0.27;
			lamp_hsv.value      = 1.0; 
			var lamp_bright = lamp_hsv.to_rgba ();
			lamp_hsv.saturation = 0.66;
			lamp_hsv.value      = 0.67; 
			var lamp_dark  = lamp_hsv.to_rgba ();

			cr.set_source (create_gradient (20, 20, 89, 87, lamp_bright, lamp_dark));
			cr.set_line_width (progress_width);
			cr.arc (xc, yc, progress_radius, start_angle, value_angle);
			cr.stroke ();

			// ProgressShadow 
			// TODO (Performace ?)

			cr.set_line_cap (LineCap.ROUND);
			var progress_rim_width = 2.0;
			cr.set_line_width (progress_rim_width);

			// ProgressBeginRim
			cr.set_source (create_gradient_str (18, 79, 35, 79, "#dfd5c9", "#dfd5c9", 1.0, 0.0));
			cr.move_to (xc + progress_radius_outer * start_angle_x, 
			            yc + progress_radius_outer * start_angle_y);
			cr.line_to (xc + progress_radius_inner * start_angle_x, 
			            yc + progress_radius_inner * start_angle_y);
			cr.stroke ();

			// ProgressEndRim
			set_source_color_string (cr, "#b3a190");
			cr.move_to (xc + progress_radius_outer * end_angle_x, 
			            yc + progress_radius_outer * end_angle_y);
			cr.line_to (xc + progress_radius_inner * end_angle_x, 
			            yc + progress_radius_inner * end_angle_y);
			cr.stroke ();

			// ProgressRim
			cr.set_source (create_gradient_str (95, 6, 5, 44, "#dfd5c9", "#b0a090"));
			cr.arc (xc, yc, progress_radius_outer, start_angle, end_angle);
			cr.stroke ();

			cr.set_line_cap (LineCap.BUTT);

			// ProgressLampReflection
			// TODO?: blur 2.0
			cr.set_source (create_gradient (20, 20, 89, 87, lamp_bright, lamp_dark, 0.25, 0.25));
			cr.set_line_width (progress_width);
			cr.arc (xc, yc, progress_radius, start_angle, value_angle + Math.PI / 180.0);
			cr.stroke ();
			
			// ProgressBackgroundShine

			Cairo.Pattern progress_shine = create_gradient_str (89, 73, 34, 16, "#ffffff", "#ffffff", 0.3, 0.04);
			progress_shine.add_color_stop_rgba (0.5,  1.0, 1.0, 1.0, 0.0);
			progress_shine.add_color_stop_rgba (0.75, 1.0, 1.0, 1.0, 0.3);

			cr.set_source (progress_shine);
			cr.set_line_width (progress_width);
			cr.arc (xc, yc, progress_radius, start_angle, end_angle);
			cr.stroke ();

			// KnobShadow
			// TODO?: blur 2.0
			/*
			cr.set_line_width (progress_rim_width);
			cr.set_source (create_gradient_str (86, 77, 38, 25, "#151928", "#151928", 1.0, 0.5));
			cr.arc (xc, yc, progress_radius_inner, 0, 2 * Math.PI);
			cr.stroke ();
			*/
			
			// KnobRimPlain
			cr.set_line_width (1.0);
			cr.set_source_rgb (0.0, 0.0, 0.0);
			cr.arc (xc, yc, progress_radius_inner, 0, 2 * Math.PI);
			cr.set_source (create_gradient_str (35, 31, 75, 72, "#68625c", "#44494b"));
			cr.fill ();
			cr.set_source_rgb (0, 0, 0);
			cr.arc (xc, yc, progress_radius_inner, 0, 2 * Math.PI);
			cr.stroke ();

			// KnobDiscBackground
			cr.set_source (create_gradient_str (42, 34, 68, 70, "#e7ecef", "#9cafb8"));
			cr.arc (xc, yc, knob_disc_radius, 0, 2 * Math.PI);
			cr.fill ();
			cr.set_line_width (2.0);

			double degrees = Math.PI / 180.0;

			// KnobRimShadow
			// TODO

			// KnobShineBright
			cr.set_source (create_gradient_str (38, 34, 70, 68, "#ffffff", "#caddf2", 0.2, 0.2));
			cr.move_to (xc,yc);
			cr.arc (xc, yc, knob_disc_radius - 1, -154 * degrees, -120 * degrees);
			cr.move_to (xc,yc);
			cr.arc (xc, yc, knob_disc_radius - 1, Math.PI / 2.0 - 60 * degrees, Math.PI / 2.0 - 29 * degrees);
			cr.fill ();

			// KnobShineDark
			cr.set_source (create_gradient_str (50, 40, 62, 60, "#a1adb6", "#47535c", 0.07, 0.15));
			cr.move_to (xc,yc);
			cr.arc (xc, yc, knob_disc_radius - 1, - 67 * degrees, - 27 * degrees);
			cr.move_to (xc,yc);
			cr.arc (xc, yc, knob_disc_radius - 1, Math.PI - 67 * degrees, Math.PI- 27 * degrees);
			cr.fill ();

			// KnobShineRipples
			Cairo.Pattern knob_ripples = new Cairo.Pattern.radial (xc, yc, 0, xc, yc, 4);
			add_color_stop_str (knob_ripples, 0.0,  "#e7ecef", 0.05);
			add_color_stop_str (knob_ripples, 0.5,  "#58717d", 0.05);
			add_color_stop_str (knob_ripples, 0.75, "#d1d9de", 0.05);
			add_color_stop_str (knob_ripples, 1.0,  "#5d7682", 0.05);
			knob_ripples.set_extend(Cairo.Extend.REPEAT);

			cr.set_line_width (0.0);
			cr.set_source (knob_ripples);
			cr.arc (xc, yc, knob_disc_radius, 0, 2 * Math.PI);
			cr.fill ();

			cr.save();

			cr.translate (xc + knob_disc_radius * value_x, 
						  yc + knob_disc_radius * value_y);

			cr.rotate (value_angle - Math.PI);

			// KnobIndicatorBay
			cr.set_source (create_gradient_str (16, -2, 9, 13, "#e7ecef", "#9cafb8", 0.8, 0.8));
			cr.move_to (0, 4);
			cr.line_to (17, 4);
			cr.curve_to (19,  4, 21, 2,  21, 0);
			cr.curve_to (21, -2, 19, -4,  17, -4);
			cr.line_to (0, -4);
			cr.close_path();			
			cr.fill();

			// KnobIndicator
			cr.set_source (create_gradient_str (9, -2, 9, 2, "#68625c", "#44494b"));
			cr.move_to (0, 2);
			cr.line_to (16, 2);
			cr.curve_to (17,  2, 18, 1,  18, 0);
			cr.curve_to (18, -1, 17, -2, 16, -2);
			cr.line_to (0, -2);
			cr.close_path();
			cr.fill();

			cr.restore ();

			// KnobDiscRim
			cr.set_line_width (2.0);
			cr.set_source (create_gradient_str (38, 32, 70, 67, "#3d3d3d", "#000000"));
			cr.arc (xc, yc, knob_disc_radius, 0, 2 * Math.PI);
			cr.stroke ();

			cr.restore ();
		} 
	} 
} // namespace Prolooks