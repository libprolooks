/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;
using Gdk;

namespace Prolooks {

public class TransportButton : ButtonBase {

    public enum IconType {
            STOP,
            PLAY,
            REC,
            FAST_FORWARD,
            FAST_BACKWARD,
            TO_END,
            TO_START
        }
    
    private IconType _icon_type;
    [Description(nick="icon type", blurb="determines, which icon the button will display, also determines whether it works like a press or toggle button: REC and PLAY work as toggle buttons, all others als press buttons")]
    public IconType icon_type {
        get {
            return _icon_type;
        } 
        set {
            if (value != _icon_type) {
                switch (value) {
                    case IconType.REC:           
                    case IconType.PLAY:          button_type = ButtonType.TOGGLE_BUTTON; break;
                    case IconType.STOP:          
                    case IconType.FAST_FORWARD:  
                    case IconType.FAST_BACKWARD:
                    case IconType.TO_END:        
                    case IconType.TO_START:      button_type = ButtonType.PRESS_BUTTON;  break;
                    default:                     GLib.assert_not_reached ();             break; 
                }
                _icon_type = value;
                queue_draw ();
            }
        }
    }
    
    public static void icon_size (IconType type, out int width, out int height) {
        switch (type) {
            case IconType.STOP:          width = 12;   height = 11;   break;
            case IconType.REC:           width = 13;   height = 13;   break;
            case IconType.PLAY:          width = 10;   height = 17;   break;
            case IconType.FAST_FORWARD:  
            case IconType.FAST_BACKWARD: width = 11;   height = 12;   break;
            case IconType.TO_END:        
            case IconType.TO_START:      width = 10;   height = 12;   break;
            default:                     GLib.assert_not_reached ();  
        }
    }
        
    construct {
        add_events (Gdk.EventMask.BUTTON_PRESS_MASK
          | Gdk.EventMask.BUTTON_RELEASE_MASK);
        
        icon_type = IconType.PLAY;
        set_size_request (30, 30);
		draw.connect(on_draw);
    }
    
    public static void paint_stop_icon (Cairo.Context cr) {
        RGBA stop_icon_color_center   = rgba_shade(rgba_from_string ("#184b6a"), 1.4);
        RGBA stop_icon_color_outside  = rgba_shade(rgba_from_string ("#050f2a"), 1.4);
        
        Cairo.Pattern pt = new Cairo.Pattern.radial (4.5, 4.5, 0, 4.5, 4.5, 3);
        add_color_stop (pt, 0,    stop_icon_color_center);
        add_color_stop (pt, 1.0,  stop_icon_color_outside);
        cr.set_source  (pt);
        
        cr.rectangle (0.5, 0, 8, 8);
        cr.fill ();
    }
    
    public static void paint_play_icon (Cairo.Context cr) {
        RGBA play_icon_color_middle  = rgba_from_string ("#4b9a0d");
        RGBA play_icon_color_outside = rgba_from_string ("#2e4f18");
        
        Cairo.Pattern pt = new Cairo.Pattern.radial (3.5, 7.5, 3, 3.5, 7.5, 10);
        add_color_stop(pt, 0, play_icon_color_middle);
        add_color_stop(pt, 1, play_icon_color_outside);
        cr.set_source (pt);
        
        cr.new_path();
        cr.move_to (0.5, 0.5);
        cr.line_to (7.5, 7.0);
        cr.line_to (0.5, 13.5);
        cr.close_path ();
        cr.fill ();
    
    }

    public static void paint_rec_icon (Cairo.Context cr) {
        RGBA rec_icon_color_middle  = rgba_shade(rgba_from_string ("#8b2f20"), 1.4);
        RGBA rec_icon_color_outside = rgba_shade(rgba_from_string ("#47090a"), 1.4);
        
        Cairo.Pattern pt = new Cairo.Pattern.radial (5.0, 5.0, 0, 5.0, 5.0, 10);
        add_color_stop(pt, 0, rec_icon_color_middle);
        add_color_stop(pt, 1, rec_icon_color_outside);
        cr.set_source (pt);
        
        cr.arc (5.0, 5.0, 5.0, 0, 2.0 * Math.PI);
        cr.fill ();
    }

    public static RGBA ff_icons_color = rgba_from_string ("#212121");
            
    public static void paint_fast_forward_icon (Cairo.Context cr) {
        set_source_color (cr, ff_icons_color);
        
        cr.save ();
        cr.translate (-1.0, -1.0);
        cr.new_path();
        cr.move_to (0, 0);
        cr.line_to (5.5, 5.5);
        cr.line_to (0, 11);
        cr.close_path ();
        cr.fill ();
                
        cr.translate (5, 0);
        cr.new_path();
        cr.move_to (0, 0);
        cr.line_to (5.5, 5.5);
        cr.line_to (0, 11);
        cr.close_path ();
        cr.fill ();
        cr.restore ();
    }

    public static void paint_fast_to_end_icon (Cairo.Context cr) {
        set_source_color (cr, ff_icons_color);
        cr.save ();
        cr.translate (0.5, -1.0);
        cr.new_path();
        cr.move_to (0, 0);
        cr.line_to (5.5, 5.5);
        cr.line_to (0, 11);
        cr.close_path ();
        cr.fill ();
        cr.translate (5, 0);
        cr.rectangle (0, 0.75, 2, 9.5);
        cr.fill ();
        cr.restore ();
    }

    public static void paint_fast_backward_icon (Cairo.Context cr) {
        cr.save ();
        cr.scale (-1, 1);
        cr.translate (-8, 0);
        paint_fast_forward_icon (cr);
        cr.restore ();
    }

    public static void paint_fast_to_start_icon (Cairo.Context cr) {
        cr.save ();
        cr.scale (-1, 1);
        cr.translate (-7, 0);
        paint_fast_to_end_icon (cr);
        cr.restore ();
    }
    
    private void paint_icon (Cairo.Context cr) {
        // icons
        int w;
        int h;
        
        icon_size (icon_type, out w, out h);

		Gtk.Allocation allocation;
		get_allocation(out allocation);
            
        cr.save ();
        cr.translate (((allocation.width - 3) / 2.0) - (w / 2.0), ((allocation.height - 3) / 2.0) - (h / 2.0));

        switch (icon_type) {
            case IconType.STOP:          paint_stop_icon (cr);            break;
            case IconType.REC:           paint_rec_icon (cr);             break;
            case IconType.PLAY:          paint_play_icon (cr);            break;
            case IconType.FAST_FORWARD:  paint_fast_forward_icon (cr);    break;
            case IconType.FAST_BACKWARD: paint_fast_backward_icon (cr);   break;
            case IconType.TO_END:        paint_fast_to_end_icon (cr);     break;
            case IconType.TO_START:      paint_fast_to_start_icon (cr);   break;
            default:                     GLib.assert_not_reached ();                  break;
        }
        cr.restore ();
    }
    
    private RGBA pressed_color () {
        RGBA pressed_color = rgba_from_string ("#c2c2c2");
        switch (icon_type) {
            case IconType.STOP:          pressed_color = rgba_from_string ("#2d73a2"); break;
            case IconType.REC:           pressed_color = rgba_from_string ("#d46552"); break;
            case IconType.PLAY:          pressed_color = rgba_from_string ("#87bb36"); break;
            case IconType.FAST_FORWARD:  
            case IconType.FAST_BACKWARD: 
            case IconType.TO_END:        
            case IconType.TO_START:      pressed_color = rgba_from_string ("#636363"); break;
            default:                     GLib.assert_not_reached ();                       break;
        }
        return pressed_color;
    }

    /* Widget is asked to draw itself */
	public bool on_draw (Cairo.Context cr) {   
		Gtk.Allocation allocation;
		get_allocation(out allocation);
     
        cr.set_line_width (1.0);
        
        cr.set_source_rgba (0.0, 0.0, 0.0, 0.0);
        cr.rectangle (0.0, 0.0, allocation.width, allocation.height);
        cr.fill ();

        set_source_color (cr, gdk_color_to_rgba(style.bg[(int)Gtk.StateType.NORMAL]));
        cr.rectangle (0, 0, allocation.width, allocation.height);
        cr.fill ();
        
        // border line
        set_source_color_string (cr, "#808080");
        cr.move_to (2.5, 1.5);
        cr.line_to (allocation.width - 2.5, 1.5);
        cr.line_to (allocation.width - 1.5, 2.5);
        cr.line_to (allocation.width - 1.5, allocation.height - 1.5);
        cr.line_to (2.5, allocation.height - 1.5);
        cr.line_to (1.5, allocation.height - 2.5);
        cr.line_to (1.5, 2.5);
        cr.line_to (2.5, 1.5);
        cr.stroke();

        // top / left shine line
        set_source_color_string (cr, "#ffffff");
        cr.move_to (allocation.width - 2.5, 2.5);
        cr.line_to (2.5, 2.5);
        cr.line_to (2.5, allocation.height - 3);
        cr.stroke ();

        // bottom / right shadow line
        set_source_color_string (cr, "#c2c2c2");
        cr.move_to (allocation.width - 2.5, 2.5);
        cr.line_to (allocation.width - 2.5, allocation.height - 2.5);
        cr.line_to (2.5, allocation.height - 2.5);
        cr.stroke ();

        // paint icon
        cr.save ();
            cr.translate (3, 3);
            
			// button base color
			if (button_state == ButtonState.PRESSED) {
				set_source_color (cr, pressed_color ());
				cr.rectangle (-1, -1, allocation.width - 3, allocation.height - 3);
			} else {
				set_source_color_string (cr, "#e4e4e4");
				cr.rectangle ( 0,  0, allocation.width - 6, allocation.height - 6);
			}
            
            cr.fill ();
                        
            paint_icon (cr);
        cr.restore (); 
        
        double shine_alpha_start = 0.8;
        double shine_alpha_stop  = 0.10;
        // draw shine
        
        // upper shine
        RGBA shine_upper_gradient1 = rgba_from_string ("#ffffff");
        RGBA shine_upper_gradient2 = rgba_from_string ("#c0c0c0");
        Cairo.Pattern shine_upper = create_gradient (0, 0, 0, allocation.height / 2.0, shine_upper_gradient1, shine_upper_gradient2, shine_alpha_start, shine_alpha_stop);
        cr.set_source (shine_upper);
        cr.rectangle (0, 0, allocation.width, allocation.height / 2.0);
        cr.fill ();
        
        // shine line
        if (button_state != ButtonState.PRESSED) {
            set_source_color_string (cr, "#ffffff", shine_alpha_stop + 0.05);
            cr.move_to (1, allocation.height / 2.0);
            cr.line_to (allocation.width - 2, allocation.height / 2.0);
            cr.stroke ();
        }
        
        // lower shine
        RGBA shine_lower_gradient1 = rgba_from_string ("#a0a0a0"); 
        RGBA shine_lower_gradient2 = rgba_from_string ("#a0a0a0"); 
        Cairo.Pattern shine_lower = create_gradient (0, allocation.height / 2.0, 0, allocation.height, shine_lower_gradient1, shine_lower_gradient2, shine_alpha_stop, shine_alpha_start);
        cr.set_source (shine_lower);
        cr.rectangle (2, allocation.height / 2.0, allocation.width - 3, allocation.height / 2.0 - 2);
        cr.fill ();
        
        return false;
    }
}

} // namespace Prolooks
