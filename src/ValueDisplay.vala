/* 
    Copyright 2009 by Hans Baier
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

public class ValueDisplay : DisplayBase {

    private string _text = "";
    public new string text {
        set {
            if (_text != value) {
                _text = value;
                queue_draw ();
            }
        }
        get {
            return _text;
        }
    }
    
    private new int width  = 36;
    private new int height = 16;
    
    construct {
        set_size_request (width, height);
        show_glass_rim = false;
    }

    protected override bool draw_contents(Cairo.Context cr) {
        // draw text
        double font_size         = inner_height;
        
        cr.select_font_face ("FreeSans", Cairo.FontSlant.NORMAL, Cairo.FontWeight.NORMAL);
        cr.set_font_size (font_size);
        Cairo.TextExtents ext = Cairo.TextExtents();
        cr.text_extents (text, out ext);
        double x = (inner_width - ext.x_advance) / 2.0;
        double y = inner_height * 0.85;
        
        DisplayBase.text (cr, text, x, y, font_size);

        return true;
    }
}

} // namespace Prolooks

