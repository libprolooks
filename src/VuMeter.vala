/* 
    Copyright 2009 by Hans Baier, Krzysztof Foltman
    License: LGPLv2+ 
*/

using Gtk;

namespace Prolooks {

public class VuMeter : DrawingArea {

    public enum Mode { 
        STANDARD,
        MONOCHROME,
        MONOCHROME_REVERSE
    }
    
    private Cairo.Surface? cache_surface = null;
    private Mode _mode = Mode.STANDARD; private double _value = 0.5;
    
    
    [Description(blurb="the displayed value, ranging from 0.0 to 1.0")]
    public double value {
        set {
            if (Math.fabs(_value - value) >= 0.0001)
                queue_draw();
            _value = value;
        }
        get {
            return _value;
        }
    }
    
    private double r = 1;
    private double g = 1;
    private double b = 0;
    
    [Description(nick="monochrome color", blurb="sets the meter color in monochrome color mode, can contain a string as accepted by gdk_rgba_parse (), eg: #a0b1c2")]
    public string color {
        set {
            if (value != null & value != "") {
	        Gdk.RGBA rgba = Gdk.RGBA();
		rgba.parse(value);
		r = rgba.red;
		g = rgba.green;
		b = rgba.blue;                

                cache_surface = null;
                queue_draw ();
            } else {
                r = 1; g = 1; b = 0; 
            }
        }
    }
    
    [Description(nick="mode", blurb="whether the vu meter should be red/yellow/green (standard), or one color (monochrome) or one color but flipped left to right (monochrome reversed)")]
    public Mode mode {
        set {
            if (value != _mode) {
                _mode = value;
                cache_surface = null;
                queue_draw();
            }
        }
        get {
            return _mode;
        }
    }
    
    construct {
		draw.connect(on_draw);
		size_allocate.connect(on_size_allocate);
		set_size_request (100, 14);
    }

	public new virtual void get_preferred_width (out int minimum_width, out int natural_width) {
        minimum_width  = 100;		
        natural_width  = minimum_width;		
	}

	public new virtual void get_preferred_width_for_height (int height, out int minimum_width, out int natural_width) {
        minimum_width  = 100;		
        natural_width  = minimum_width;				
	}

	public new virtual void get_preferred_height_for_width (int width, out int minimum_height, out int natural_height) {
		minimum_height = 14;
		natural_height = minimum_height;		
	}

 	public new virtual void get_preferred_height (out int minimum_height, out int natural_height) {
		minimum_height = 14;
		natural_height = minimum_height;
	}
    
    public void on_size_allocate (Gtk.Allocation allocation) {
		set_allocation(allocation);

        cache_surface = null;
    
        if (get_realized ()) {
            get_window().move_resize (allocation.x, allocation.y, allocation.width, allocation.height);
        }
    }
    
    /* Widget is asked to draw itself */
    public bool on_draw (Cairo.Context cr) {    
		Gtk.Allocation allocation;
		get_allocation(out allocation);     
    
        int ox = 1, oy = 1;
        int sx = allocation.width - 2, sy = allocation.height - 2;

        if (cache_surface == null)
        {
            int x;
            // looks like its either first call or the widget has been resized.
            // create the cache_surface.
            cache_surface = new Cairo.Surface.similar (cr.get_target(), 
                                                       Cairo.Content.COLOR,
                                                       allocation.width,
                                                       allocation.height );

            // And render the meterstuff again.
        
            Cairo.Context cache_cr = new Cairo.Context ( cache_surface );
            cache_cr.set_source_rgba (0, 0, 0, 0);
            cache_cr.rectangle (ox, oy, sx, sy);
            cache_cr.fill ();
            cache_cr.set_line_width (1);

            for (x = ox; x <= ox + sx; x += 3)
            {
                double ts = (x - ox) * 1.0 / sx;
                switch(mode)
                {
                    case Mode.STANDARD:
                    default:
                        if (ts < 0.75) {
                            r = ts / 0.75;
                            g = 1;
                            b = 0;
                        }
                        else {
                            r = 1;
                            g = 1 - (ts - 0.75) / 0.25;
                            b = 0;
                        }
                        break;
                    case Mode.MONOCHROME_REVERSE:
                        break;
                    case Mode.MONOCHROME:
                        break;
                }
                cache_cr.set_source_rgb(r, g, b);
                cache_cr.move_to (x, oy);
                cache_cr.line_to (x, oy + sy + 1);
                cache_cr.move_to (x, oy + sy);
                cache_cr.line_to (x, oy );
                cache_cr.stroke  ();
            }
        }

        cr.set_source_surface (cache_surface, 0, 0);
        cr.paint ();
        cr.set_source_rgba (0, 0, 0, 0.5);

        if( mode == Mode.MONOCHROME_REVERSE )
            cr.rectangle (ox, oy, _value * (sx - ox) + 1, sy - oy + 1);
        else
            cr.rectangle (ox + _value * (sx - ox), oy, sx - ox + 1, sy - oy + 1);

        cr.fill ();
        
        return false;
    }
}

} // namespace Prolooks
