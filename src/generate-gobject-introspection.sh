#!/bin/bash
g-ir-scanner --warn-all -I/usr/include/glib-2.0/ -I/usr/lib/i386-linux-gnu/glib-2.0/include/ -I/usr/include/gtk-2.0/ -I/usr/include/cairo/ -I/usr/include/pango-1.0/ -I/usr/lib/i386-linux-gnu/gtk-2.0/include/ prolooks.h -I/usr/include/gdk-pixbuf-2.0/ -I/usr/include/atk-1.0/ -n Prolooks --nsversion 1.2 --include GObject-2.0 --include Gtk-2.0 --library libprolooks-1.2.la -o Prolooks-1.2.gir
 g-ir-compiler -o Prolooks-1.2.typelib Prolooks-1.2.gir 
