/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * pixman.vapi
 * Copyright (C) Nicolas Bruguier 2009 <gandalfn@club-internet.fr>
 * 
 * maiawm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * maiawm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[CCode (cprefix = "Pixman", lower_case_cprefix = "pixman_", cheader_filename = "pixman.h")]
namespace Pixman 
{
	[SimpleType]
	[CCode (cname = "pixman_fixed_t")]
	[IntegerType (rank = 6)]
	public struct Fixed {
		[CCode (cname = "pixman_fixed_to_double")]
		public Pixman.double to_double();
		[CCode (cname = "pixman_fixed_to_int")]
		public Pixman.int to_int();
		[CCode (cname = "pixman_fixed_ceil")]
		public Fixed ceil();
	}

	[CCode (cname = "int")]
	[IntegerType (rank = 6)]
	public struct int {
		[CCode (cname = "pixman_int_to_fixed")]
		public Fixed to_fixed();
	}

	[CCode (cname = "double", default_value = "0.0")]
	[FloatingType (rank = 2)]
	public struct double {
		[CCode (cname = "pixman_double_to_fixed")]
		public Fixed to_fixed();
	}

	[CCode (cname = "pixman_box32_t")]
	public struct Box32 {
		public int32 x1;
		public int32 y1;
		public int32 x2;
		public int32 y2;
	}

	[CCode (cname = "pixman_region32_t")]
	public struct Region32 {
		public Box32 extents;

		public void init ();
		public void init_rect (int x, int y, uint width, uint height);
		public bool init_rects (Box32 boxes, int count);
		public void init_with_extents (Box32 extents);
		public void fini ();

		public bool copy (Region32 inRegion);

		public bool not_empty ();
		[CCode (cname = "pixman_region32_extents")]
		public unowned Box32* get_extents();
		public unowned Box32[] rectangles();
		public bool subtract (Region32 reg_m, Region32 reg_s);
		public bool union (Region32 reg_m, Region32 reg_s);
		public bool intersect (Region32 reg_m, Region32 reg_s);
	}

	[CCode (cname = "pixman_filter_t")]
	public enum Filter {
		FAST,
		GOOD,
		BEST,
		NEAREST,
		BILINEAR,
		CONVOLUTION
	}

	[CCode (cname = "pixman_op_t")]
	public enum Op {
		CLEAR,
		SRC,
		DST,
		OVER,
		OVER_REVERSE,
		IN,
		IN_REVERSE,
		OUT,
		OUT_REVERSE,
		ATOP,
		ATOP_REVERSE,
		XOR,
		ADD,
		SATURATE,

		DISJOINT_CLEAR,
		DISJOINT_SRC,
		DISJOINT_DST,
		DISJOINT_OVER,
		DISJOINT_OVER_REVERSE,
		DISJOINT_IN,
		DISJOINT_IN_REVERSE,
		DISJOINT_OUT,
		DISJOINT_OUT_REVERSE,
		DISJOINT_ATOP,
		DISJOINT_ATOP_REVERSE,
		DISJOINT_XOR,

		CONJOINT_CLEAR,
		CONJOINT_SRC,
		CONJOINT_DST,
		CONJOINT_OVER,
		CONJOINT_OVER_REVERSE,
		CONJOINT_IN,
		CONJOINT_IN_REVERSE,
		CONJOINT_OUT,
		CONJOINT_OUT_REVERSE,
		CONJOINT_ATOP,
		CONJOINT_ATOP_REVERSE,
		CONJOINT_XOR,

		MULTIPLY,
		SCREEN,
		OVERLAY,
		DARKEN,
		LIGHTEN,
		COLOR_DODGE,
		COLOR_BURN,
		HARD_LIGHT,
		SOFT_LIGHT,
		DIFFERENCE,
		EXCLUSION,
		HSL_HUE,
		HSL_SATURATION,
		HSL_COLOR,
		HSL_LUMINOSITY,

		NONE
	}

	[CCode (cname = "pixman_format_code_t", cprefix = "PIXMAN_")]
	public enum Format {
		a8r8g8b8,
		x8r8g8b8,
		a8b8g8r8,
		x8b8g8r8,
		b8g8r8a8,
		b8g8r8x8,
		x2r10g10b10,
		a2r10g10b10,
		x2b10g10r10,
		a2b10g10r10,

		/* 24bpp formats */
		r8g8b8,
		b8g8r8,
    
		/* 16bpp formats */
		r5g6b5,
		b5g6r5,
    
		a1r5g5b5,
		x1r5g5b5,
		a1b5g5r5,
		x1b5g5r5,
		a4r4g4b4,
		x4r4g4b4,
		a4b4g4r4,
		x4b4g4r4,
    
		/* 8bpp formats */
		a8,
		r3g3b2,
		b2g3r3,
		a2r2g2b2,
		a2b2g2r2,
    
		c8,
		g8,
    
		x4a4,
    
		x4c4,
		x4g4,
    
		/* 4bpp formats */
		a4,
		r1g2b1,
		b1g2r1,
		a1r1g1b1,
		a1b1g1r1,
    
		c4,
		g4,
    
		/* 1bpp formats */
		a1,    
		g1,

		/* YUV formats */
		yuy2,
		yv12
	}

	[Compact]
	[CCode (ref_function = "pixman_image_ref", unref_function = "pixman_image_unref", cname = "pixman_image_t")]
	public class Image {
		[CCode (cname = "pixman_image_create_bits")]
			public Image.for_bits (Format format, int width, int height, [CCode (array_length = false)] uchar[] bits, int rowstride_bytes);

		[CCode (cname = "pixman_image_set_filter")]
		public int set_filter (Pixman.Filter filter, [CCode (array_length = false)] Pixman.Fixed[] params, int n_filter_params);
	}	

	[CCode (cname = "pixman_image_composite")]
	public static void image_composite (Op op, Image src, Image? mask, Image dest, int16 src_x, int16 src_y, int16 mask_x, int16 mask_y, int16 dest_x, int16 dest_y, uint16 width, uint16 height);

}
